clear all;
close all;
addpath('~/bin/matlab2tikz/src');

%init scales

RES = 2^15-1;
L = 0.256;
W = 0.04;
res = [L/RES L/RES L/RES];
x = 0:res(1):L;
y = -W/2:res(2):W/2;
%z = -W/2:res(2):W/2;
z = 0;
% init frequency
omega = 1e6:1e3:4e6;
omega = 2*pi*omega;
Aomega = zeros(size(omega))+1/length(omega);

%init slowness/sos/freq 
sos = 1500;
fc = 2.5e6;
lambda = sos/fc;
r = sqrt(lambda*L)/2;

slowness = 1/sos; 


%distance computations.
[xGrid,yGrid,zGrid] = meshgrid(x,y,z);
startPt = [x(1),y(floor(end/2)),z(ceil(end/2))] ;
endPt = [x(floor(end)),y(floor(end/2)),z(ceil(end/2))];
dse = norm(startPt-endPt,2);

xGridsel = xGrid(end/2);
yGridsel = yGrid(:,end/2);
zGridsel = zGrid(:,end/2);


DS = sqrt((xGridsel-startPt(1)).^2+(yGridsel-startPt(2)).^2+(zGridsel-startPt(3)).^2);
DR = sqrt((xGridsel-endPt(1)).^2+(yGridsel-endPt(2)).^2+(zGridsel-endPt(3)).^2);

delta = DS+DR-dse;

fresnelZone = delta < 1500/(2*2.5e6);

%frechet building:

frac = (slowness/(2*pi))*dse./(DS.*DR);
TMap = slowness.*(DS+DR-dse);

sinmap2DT = zeros(size(TMap));
sinmap3DT = zeros(size(TMap));
sinmap2DA = zeros(size(TMap));
sinmap3DA = zeros(size(TMap));

for k=1:length(omega)
    sinmap2DT = sinmap2DT + Aomega(k)*(omega(k)^0.5)*sin(omega(k)*TMap+pi/4);
    sinmap3DT = sinmap3DT + Aomega(k)*(omega(k)^1.0)*sin(omega(k)*TMap);
    sinmap2DA = sinmap2DA + Aomega(k)*(omega(k)^1.5)*cos(omega(k)*TMap);
    sinmap3DA = sinmap3DA + Aomega(k)*(omega(k)^2.0)*cos(omega(k)*TMap);
end



kernel2DT = sqrt(frac).*sinmap2DT;
kernel3DT = frac.*sinmap3DT;
kernel2DA = sqrt(frac).*sinmap2DA;
kernel3DA = frac.*sinmap3DA;


kernel2DT = kernel2DT/nansum(kernel2DT(:));
kernel3DT = kernel3DT/nansum(kernel3DT(:));
kernel2DA = kernel2DA/nansum(kernel2DA(:));
kernel3DA = kernel3DA/nansum(kernel3DA(:));

linkern = 1 - 2*delta/lambda;
linkern(~fresnelZone) = 0;
linkern = linkern/sum(linkern);
          
% tofTotal = min(TS(:)+TR(:));
% TMap = TS + TR - tofTotal;
% 
% fatRay = (TMap < Tcutoff/2);
% weightMap = 1-(2.*fcutoff.*TMap);
% weightMap(~fatRay) = 0;
% weightMap = (weightMap ./ sum(weightMap(:))) .*dse;

y = y *1000;
r = r * 1000;
figure;
plot(y,kernel3DT);
hold on;
plot(y,kernel2DT);
plot(y,linkern);

yvals = get(gca,'ylim');
plot([-r,-r],yvals,'--k');
plot([r,r],yvals,'--k');

xlabel('radial distance');

% figure;
% plot(y,kernel3DA);
% hold on;
% plot(y,kernel2DA);
% plot(y,linkern);
% 
% plot([-r,-r],yvals,'--k');
% plot([r,r],yvals,'--k');
% 
% xlabel('radial distance');

%% undersampling
for k=[6,7,8,9]
figure;
% subplot(2,2,k-5);
plot(y(1:2^k:end),kernel3DT(1:2^k:end),'.-');
hold on;
plot(y(1:2^k:end),kernel2DT(1:2^k:end),'.-');
plot(y(1:2^k:end),linkern(1:2^k:end),'.-');

yvals = get(gca,'ylim');
plot([-r,-r],yvals,'--k');
plot([r,r],yvals,'--k');
title(sprintf('res: %i',ceil(RES/2^k)));
xlabel('radial distance');
ylabel('normalized amplitude');
% cleanfigure;
% matlab2tikz(sprintf('kernel_res_%i.tex',ceil(RES/2^k)));
end



%%


% figure;
% h=slice(xGrid,yGrid,zGrid,kernel3D,[x(end/4) x(end/2) x(3*end/4)],y(end/2), z(end/2));
% xlabel('x');ylabel('y');zlabel('z');
% axis equal;
% set(h,'linestyle','none')
% 
% figure;
% xyslice = kernel3D(:,:,end/2);
% imagesc(squeeze(xyslice));
% axis image;
% 
% figure;
% xyslice = kernel3D(:,end/2,:);
% imagesc(squeeze(xyslice));
% axis image;
