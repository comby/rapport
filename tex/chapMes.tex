\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Numerical Analysis}
\label{chap:mes}
  In \autoref{chap:ray} we investigated the use of a forward linear operator (our ray Matrix) and how to fill it.
The overall reconstruction process can be viewed as  an iterative scheme, summarized by \autoref{fig:trans_scheme}.

This Chapter focused on a numerical analysis of the elaborated forward model , and how it is influenced by external parameters. These insights will later help to choose a suitable solver for the reconstruction, and also shows the need for a regularisation procedure in the solver.

Without further information, reconstruction presented here will be done with the standart SART solver, and the reconstruction will be compare in term of SNR (see \eqref{eq:SNR}) to a groundtruth obtained by scaling down the breast phantom used for the simulation to the reconstruction resolution (128x128)

\begin{equation}
\label{eq:SNR}
  SNR = \sqrt{ \frac{\sum_{k}A_{k}^{2}}{\sum_{k}(A_{k}-B_{k})^{2}}}
\end{equation}

where $A$ is the ground-truth image and $B$ the reconstructed one.

\section{Matrix construction}
\subsection{Sparsity}
A good reconstruction via a solver (SART or TVAL) is only possible if the matrix provided is a ``good'' matrix. For characterizing it different estimators can be used about the information encoded in this matrix. Straight ray and bent ray have similar sparsity, and only the case of straight ray  are represented on \autoref{fig:spar} to \ref{fig:ray_hits2}.

\begin{figure}[H]
  \centering
  \setlength{\mywidth}{.5\textwidth}
  \setlength{\myheight}{.70\mywidth}
  \subcaptionbox{
    \label{fig:spar1} Influence of the opening angle}[\mywidth]{\input{plot/spar_angle}}%
  \subcaptionbox{
    \label{fig:spar2} Influence of the number of transducer}[\mywidth]{\input{plot/spar_step}}
  \caption[Sparsity of Matrix for different ray methods]{
    \label{fig:spar}Sparsity of Matrix in straight ray (SR) and fat ray from Fréchet Kernel in 2D (FR-2D) and 3D (FR-3D). Sparsity is defined as the percentage of null value inside the matrix.
  }
\end{figure}

\begin{sidewaysfigure}
  \includegraphics[width=\textwidth]{plot/ray_comp.png}
  \caption[Spatial distribution of ray (number of transducer)]{\label{fig:ray_hits}The use of fat ray greatly reduces the need of emitter, and increases the amount of information gathered per pixels. The highest hits per ray are achieved by the 2D Kernel. The reconstruction resolution is 128x128 px.}
\end{sidewaysfigure}

\begin{sidewaysfigure}
  \includegraphics[width=\textwidth]{plot/ray_comp_angle.png}
  \caption[Spatial distribution of ray (opening angle)]{\label{fig:ray_hits2}The use of fat ray greatly reduces the impact of the opening angle, and increases the amount of information gathered per pixels. For small opening angle, the overlapping area for fat ray is significantly broader.}
\end{sidewaysfigure}
\FloatBarrier{}
As it could be foreseen, the use of fat ray kernel (derived from a linear approximation or Fréchet based) greatly reduces the sparsity of the matrix. 5 times more pixels are hit by a fat ray (\autoref{fig:spar}).

Both the opening angle and the number of transducers reduce the number of rays considered probing the reconstruction area. On \autoref{fig:spar1}, the opening angle has a significant impact on the sparsity of fat ray based matrix, this effect also happen with straight rays, but is less visible, as each straight ray does not bring as much pixel hits as for.
With a small opening angle, the number of emitter-receiver pair is reduced, and thus, every fat ray account proportionally for a larger part of the matrix.
the same result on a small scale is visible on \autoref{fig:spar2}.

Ultimately it can be say that the opening angle has an important influence on the sparsity of the matrix, and will thus have after-effect on the reconstructed image.

\subsection{Conditioning of M}

The dimension of M and its sparsity makes it difficult to evaluate its condition number (computing all its non zeros singular values can take several hours on a computing server).
In addition the singular value are slowly decaying after the first hundreds ones, making the use of a classic SVD solver questionable.
We can not early cut the decomposition, as it will remove too much energy (or information) for giving a suitable results, see \autoref{fig:svd_plot}.
Roughly the last half of the singular values are very close to zero (or equal to, with numerical approximation).
The use of SVD-based solver will likely  not produced good results.

\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{.9\textwidth}
  \setlength{\myheight}{.19\textheight}
  \input{plot/svd_plot.tex}
  \caption[Singular Value distributions]{\label{fig:svd_plot} Singular Value for different number of transducers (reduce the number of row of the matrix). The normalized index is the index of the value divide by the total number of row.  }
\end{figure}


\section{Transducer limitations}
The use of the SART solver allows to easily compare the influence of the main transducer limitations: their opening angle and their number.

The choice of the regularisation parameters is left to the \texttt{AIR II} toolbox, yielding $\lambda\simeq 1.9$ in most cases. No ROI limitation of the computation have been used.

For each situation, solutions with straight rays, bent rays (based on the straight ray approximation), fat rays (independent of previous iteration) have been computed. The use of the signal-to-noise ratio (SNR) provides the largest range of value over the simulation, making it a good candidate to characterise theses solutions.

\subsection{Opening angle}

On \autoref{fig:sart_angle} the Signal-to-noise ratio highly increases for opening angle larger than \SI{30}{\degree}.
With such angles, the breast is completely covered in an area where most of the ray passed, increasing the amount of information gathered on it, and so the quality of the reconstructed image.
But having a too wide opening angle also increases reception of noisy data, which will decrease the overall SNR.


\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{.7\textwidth}
  \setlength{\myheight}{.7\mywidth}
  \input{plot/sart_angle_snr.tex}
  \caption[Influence of opening angle on SART reconstruction]{\label{fig:sart_angle} Having a wider opening angle create a wider area of overlapping rays, when the full breast is inside this area (starting at \SI{40}{\degree}). Having a very wide opening angle (over \SI{60}{\degree}) nearly improve the image, as it adds more information (potentially noisy) about the background area. BR: Bent ray, FR-L: linear interpolated fat ray
  }
\end{figure}

\subsection{Number of transducers}

The effect of different number of transducers on the various rays methods used is shown on \autoref{fig:sart_steps}, the number of ray is proportional to the square of the number of transducers, and explains the shape of these plots.
However, the use of fat ray tones down this effect, and this highlight the main advantage of fat ray, where the impact of a few transducers is decrease, because a single ray carry more information about pixels, as shown on \autoref{fig:ray_hits}.

\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{0.7\textwidth}
  \setlength{\myheight}{.707\mywidth}
  \input{plot/sart_steps_snr.tex}
  \caption[Influence of the number of transducer on SART reconstruction]{\label{fig:sart_steps} Influence of the number of transducer use for the reconstruction, with a totally open angle (\SI{90}{\degree}).}
\end{figure}

\section{Resolution Size}
Along with the Number of rays (\ie{} number of transducer squared), the number of pixel to reconstruct is the other main parameter influencing the problem size and the quality of the results.

Furthermore, the conception of the forward model, raised awareness on the smallest pixel size possible, while respecting Shannon's Theorem. In practice, we should not try to reconstruct too small pixel, which will increased the sensitivity to noise and the overall computing time.
On the other hand, the goal of USCT is to detect tumors in their early stages, and so a small grid is mandatory.

\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{0.7\textwidth}
  \setlength{\myheight}{.707\mywidth}
  \input{plot/snr_px_plot.tex}
  \caption[Influence of the resolution SART reconstruction]{\label{fig:snr_px} Influence of the pixel size  used for the reconstruction for different number rays.}
\end{figure}

\autoref{fig:snr_px} gives insight on two aspect of the reconstruction: Firstly, by using big pixels, the SNR resulting from the comparison with the downsampled version of the groundtruth (or respectively by upsampling the reconstruction) is higher for small resolution grid.
Indeed, the implicit averaging remove noises, however, this also mean we don't have access to fine details of the image.

Then, after a pixel size smaller than 1 or \SI{2}{mm} the SNR remains constant. Thus there is no need to look after the smallest pixel size computationable. In practice, the standard \(128\times 128\) grid (\SI{2}{mm} pixel width) is good enough and provides a good compromise between quality of the result and computational time.


\section{Construction time}

If the algorithms developed here are not really time critical, the future use of its in a medical device and the will of not waiting too long during the development process drive the search of a time efficient algorithm, with a low order complexity, and making good use of array parallelisation.
Furthermore, in a more business approach, reducing the computing power and memory usage will result into a cheaper device.
With such constrains, the goal is still to achieve the best reconstruction possible.

As shown in \autoref{chap:ray}, for each reconstruction based on a ray model (Straight, bent, fat, \etc)  the overall process can be divide two parts: the matrix construction (forward modelling) and the solver reconstruction (inversion).

Moreover, the matrix reconstruction time is clearly dependent on the problem size (\ie number of ray and number of pixels to reconstruct).

\subsection{Resolution grid}

  \setlength{\mywidth}{0.27\textwidth}
  \setlength{\myheight}{2\mywidth}
\begin{figure}[hbt]
  \centering
  \input{plot/matrix_px.tex}
  \caption[Matrix building time (pixels)]{\label{fig:time_matrix_px}  Time need to complete the forward modelling (matrix building) for different amount of pixels.}
\end{figure}

\autoref{fig:time_matrix_px} indicates a time proportional to the total number of pixel for every ray type. However, the straight rays construction is more than 10 faster.
This is result is the consequence of the simplicity of the method, and its impletementation, relying on the bresenham line's algorithm, done in \texttt{C} and compiled as a \texttt{mex} function.
The sole reconstruction of bent ray (without considering the previous necessary straight ray reconstruction) is slower  than the fatrays construction.
Furthermore, the 3D fat ray kernel, having a slighly simpler expression than the 2D one is faster.

By implementing the fatray methods in \texttt{C} and by using a multi-threaded algorithm (each ray Kernel is independent), one could hope to achieve similar construction time than the straight ray one.

\subsection{Number of ray}

\begin{figure}[hbt]
  \centering
  \input{plot/matrix_rays.tex}
  \caption[Matrix building time (rays)]{\label{fig:time_matrix_rays}  Time need to complete the forward modelling (matrix building).}
\end{figure}

Similarly to the resolution grid, the total number of rays use for the reconstruction also drives the matrix construction time.
However, we can see on \autoref{fig:time_matrix_rays} that a sub-linear time complexity is achieved for all non-straight rays. This effect comes in fact in hands with a slightly increased memory usage, as every ray that have the same emitter share some information: for bent rays, it is the fast marching map; for fat rays, the map of distance between this emitter and every point of the aperture.


\subsection{Use of a region of interest}

A more subtile accelerator for the forward modelling, is the usage of a Region of interest, with almost no overhead cost the computation is reduce both for pixels and rays (by ignoring all rays that does not cross this region of interest).


  \setlength{\mywidth}{0.7\textwidth}
  \setlength{\myheight}{0.707\mywidth}
\begin{figure}[hbt]
  \centering
  \input{plot/time_roi.tex}
  \caption[Construction time with ROI]{\label{fig:time_roi}  Time need to complete the forward modelling (matrix building) with restriction to a ROI, it consists of a disc center in the 2D full aperture: (\(256\times256 grid\),\num{16256} rays) with a variable radius}
\end{figure}


The rays computation react differently to a restriction of the computation to the ROI.
On \autoref{fig:time_roi}, the bent rays are almost not affected, by a reduction of the ROI, due to the need of computing the full fast marching map for each ray only after that can the path of a ray be restricted to the part where it crosses the ROI and add into the matrix. This is the same for the straight rays, but the \texttt{C} implementation reduces this effect.

On the other hand, the add of a ROI greatly reduces the computation for fat rays. The combined effect on pixels and rays can be seen on the shape of the curve for these methods, which is more quadratic than linear.


\section{Summary}

The USCT transmission reconstruction methods, built on a solid theory shows also some challenges in the numerical applications, the high sparsity of the measurement and their sensibility to various configuration of the device has been studied in 2D, and can be easily extrapolate to 3D, with the matrix being even more sparse.
Furthermore, the construction of the matrix is in itself an heavy task, both in time and memory.

Empirically, the time of reconstruction is proportional to  $n_{rays}\times n_{pixels}$.
The use of Region of interest (ROI) is of great help for reducing the time needed for computation, and it also brings a small improvement on the reconstructed images.


% \begin{figure}[hbt]
%   \centering
%   \input{plot/solver_px.tex}
%   \caption[Solver reconstructing time]{\label{fig:time_solver_px}  Time need to complete the solver reconstruction (SART) (No ROI).}
% \end{figure}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
