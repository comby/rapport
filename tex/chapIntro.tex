\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Introduction and objectives}
\label{chap:intro}

\section{Motivation}

Breast cancer is the most diagnosed cancer for women.
It represents 24\% of new diagnosed cancers and around 15\% of the death of women due to cancer.
Around 60\% of the deadly cases take place in Occident\cite{bray_global_2018,desantis_breast_2017}.

In opposition to tumours in other organs, which have already in their early stages impact on patients, breast tumours can stay hidden for a long time, until they build metastases.
Consequently, most patients does not die of the breast cancer itself, but rather from the developed metastases and their consequences.
The average size of a tumour at its detection is currently 1 cm wide, and the probability of having developed metastases at this stage is around 30\%\cite{sivaramakrishna_detection_1997}.
Those data makes it clear that detecting tumours as early as possible make a huge improvement on the chance of survival.
Current devices to detect small tumours are either very expensive (MRI) or use X-ray mammography, which can expose patients to high and repeated ionizing radiations and their consequences.
Developing methods to establish a breast cancer diagnosis with same or higher resolution is therefore very interesting. Among the different methods currently in development (low dose computed Tomography CT, photo acoustic devices\ldots), ultrasound tomography in 2D or 3D is a serious candidate for this application.

In the Institute for Data Processing and Electronics (IPE) at the Karlsruhe Institute for Technology (KIT) a complete 3D Ultrasound Computer Tomography (3D-USCT) is being developed since 2000.
Though 2D systems are also under development around the world, the 3D USCT is the only device that has the capabilities to probe completely the breast, with high isotropic resolution and reproducible 3D volume, while keeping a fast acquisition time.
Currently, a new, improved third generation of 3D-USCT is under development and a commercial application is under investigation.
Meanwhile, the second generation of the 3D-USCT prototype is also under test in a clinical study at the University hospital Mannheim.

The goal of the 3D-USCT is to be able to detect tumour smaller (and therefore sooner) than the current state of the art.
Though other devices can also detect with high resolution and in 3D such small tumours, they are  either very expensive (MRT) or use ionizing X-ray (CT).
On the other hand, ultrasound tomography is harmless for the human body, is cheaper and have a shorter examination time than MRI.
This makes the 3D-USCT a good candidate for performing regular diagnosis.
The newest version of the USCT will be able to detect tumour with a resolution of 5 mm. At this stage the probability of development of metastases is only around 5\%.\cite{sivaramakrishna_detection_1997}

\section{Principle of Ultrasound Tomography}


The groundwork provided by Greenleaf~\cite{greenleaf_clinical_1981} has opened the field of medical transmission ultrasound tomography. The principle of ultrasound tomography can be outlined as follows:

\textit{
A defined ultrasound wave is emitted by transducers, goes through the measured object and is then received by similar transducers.
As the wave goes in the object, its characteristics (amplitude and phase) are modified.
Those modifications carry information about the inside of this object (speed of sound, attenuation, etc.), In the case of probing a breast, this can be used to detect the presence of tumorous cells, as presented in \autoref{fig:cell_charts}.
}
\subsection{Proof of Concept}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{greenleaf.png}
  \caption[Ultrasonic caracterisation of breast tissues]{Relationship between ultrasonic speed and relative attenuation for several tissues of the breast. Connective tissue and some cancers  exhibit high speed and low attenuation and have been found in breasts of young women. Medium-speed cancers with high attenuation have been found in older women.\cite{greenleaf_clinical_1981}}\label{fig:cell_charts}
\end{figure}

Ultrasound provides multiple levels of information (unlike X-ray) on the object for imaging: the attenuation of the wave's sound pressure indicates on the object's attenuation coefficient, the time-of-flight of the wave gives speed of sound information, and the scattered wave indicates on the echogenicity of the object (\eg{} refraction index, surface morphology, etc.).

The choice of using Ultrasound frequency (over 20kHz) is driven by two opposite phenomena: the penetration thickness and the resolution power.
The penetration thickness decreases with higher frequency, reducing the efficacy of the device to detect deep tumours, and the resolution factor is driven by the wavelength inversely proportional to the emitted frequency: the higher the frequency, the smaller are the detected tumours.
As the emitted energy is limited by technical and medical considerations, the choice of an average frequency of \SI{2.5}{MHz} has been made.
More details about the emitted waves characteristics are available in the literature of the project \cite{gemmeke_3d_2017,derouich_vergleich_2008}.

Unlike conventional ultrasound sonography, which uses phased array technology for beam forming and reflection tomography, the 3D-USCT is based on unfocused spherical waves for imaging, taking a broader picture of the measured media.
However, the 3-USCT relies on intensive processing of the acquired data to produce images.
The continuous rise of computing power and data storage have made such device now possible.
\subsection{The 3D-USCT project}
Several Ultrasound Computer Tomographs are in development in the world~\cite{pellegretti_clinical_2011,duric_breast_2013,jirik_sound-speed_2012}, but all of them are 2D or stacks of 2D (2.5D) acquisition and reconstruction devices.
This does not able to probe the reflection out of the plane of detection like on \autoref{fig:2-3D_usct}.
The 3D-USCT of IPE is to my knowledge, the only USCT able to probe such out of the plane waves, and have an isotropic resolution.

\begin{figure}[hbt]
  \centering
  \begin{subfigure}{0.5\linewidth}
    \centering
    \input{tikz/2d_principe.tikz}
    \subcaption{2D}
  \end{subfigure}%
 \begin{subfigure}{0.5\linewidth}
    \centering
    \input{tikz/3d_principe.tikz}
    \subcaption{3D}
  \end{subfigure}%
 \caption[2D vs 3D tomography]{\label{fig:2-3D_usct}Comparison between 2D and 3D tomograph: in the 2D (a) case after passing a scattering object we only get access to in-plane and reflected signals, in a 3D USCT (b) we can catch every response signal, in and out of plan.}
\end{figure}

\subsection{General Processing}
The 3D-USCT developed at the IPE in KIT is a complex project, where design and financial constraints as well as medical norms defined strong boundaries on the field of possibilities, setting also challenges for the signal processing and image reconstruction, from the hardware and the data storage to the speed of reconstruction.
Numerous process are involved, to acquire A-scans (Acquisition of the Amplitude of a wave), process them and use the result data to reconstruct images.
These main steps are represented on \autoref{fig:gen_principle}.
Although the subject of my work is mainly the reconstruction of  transmission tomography images, having a global view over the previous signal processing steps will help understanding and improving the reconstructions step, especially for determining  uncertainty causes and  noise filtering treatment we could apply.

\begin{figure}[htb]
  \footnotesize
  \centering
  \begin{tikzpicture}[
       box/.append style={draw,
        rectangle,
        minimum width=2.5cm,
        minimum height=3em,
        inner sep=0pt,
        outer sep=0pt}
    ]

    \coordinate (A) at (0,0);
  \node[box,right= of A] (B){\begin{tabular}{c}
                A-scan \\ Acquisition
              \end{tabular}
            };
  \node[box,right= of B] (C){
              \begin{tabular}{c}
                Time-of-Flight \\ Detection
              \end{tabular}
            };
  \node[box,right= of C] (D){
              \begin{tabular}{c}
                Transmission \\ Tomography
              \end{tabular}
            };
  \node[box,right= of D] (E){
              \begin{tabular}{c}
                Reflexion \\ Tomography
              \end{tabular}
            };
  \draw[-latex] (B) -- (C);
  \draw[-latex] (C) -- (D);
  \draw[-latex] (D) -- (E);
\end{tikzpicture}
\caption{General Processing of US Tomography}
\label{fig:gen_principle}
\end{figure}


\section{Presentation of the IPE}
The Institute for Data Processing and Electronics (IPE) is specialized in the development of custom detector, trigger and data acquisition systems for high data rates and in control and monitor systems.

IPE's competences cover the entire signal chain, starting with the physical sensor design, detector assembly through the analogical and digital electronics to the data analysis and archiving.
An Electronic Packaging Laboratory is attached to the institute, where the production process is optimized and the detectors and electronic assemblies are cost-efficiently produced.

The USCT project mobilizes the core competences of the IPE with deep interaction in-between.
Designing sensors and fast acquisition board, reconstruction algorithms, hardware integration, are key elements driving the project as well as the institute. The diversity of the interacting fields with the USCT is represented on \autoref{fig:mindmap}.

\section{Objectives}
During this year of research at the IPE institute, I will be focusing my work on the medical image reconstruction from simulated data as well as experimental data.
Furthermore, my goals will be to analyse and optimize transmission tomography reconstruction algorithms in a simulated framework.
Most of my work will consist of 2D reconstruction as the 3D reconstruction algorithms can be deduced from the 2D implementation. Restraining most of the experiment in 2D give us access to faster reconstruction time, and also easier prototyping of new reconstruction methods.
The reconstruction will be implemented in MATLAB ®, but also using C function calls for time-critical operations.

\begin{figure}[hbt]
  \centering
  \input{tikz/mindmap.tikz}
  \caption[Mindmap of USCT project]{\label{fig:mindmap}Work domains of the USCT Project, my work focuses on the image transmission tomography, but extended knowledge about the data processing is also needed.}
\end{figure}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
