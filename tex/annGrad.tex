\documentclass[main.tex]{subfiles}
\begin{document}
\section{Gradient Computation}
\subsection{Left and Right approximation}
\label{sec:grad}
Gradient Computation is used multiple times in the reconstruction, especially in the Fast Marching Map Method. Classically the Gradient of an 2 variable discrete function (\eg a image) $f(i,j)$ is compute as follows, using first order finite difference, with a step size of width $h$:

\begin{equation}
  \label{eq:grad_right}
  \begin{cases}
    \hat{g_{x}}^{(r)}(x,y) = \displaystyle\frac{f(x+h,y)-f(x,y)}{h} \\[1em]
    \hat{g_{y}}^{(r)}(x,y) = \displaystyle\frac{f(x,y)-f(x,y+h)}{h}
  \end{cases}
\end{equation}

This is the right-side approximation and the same could be done with the left-side using $x-h$ and $x$:

\begin{equation}
  \label{eq:grad_left}
  \begin{cases}
    \hat{g_{x}}^{(l)}(x,y) = \displaystyle\frac{f(x+h,y)-f(x,y)}{h} \\[1em]
    \hat{g_{y}}^{(l)}(x,y) = \displaystyle\frac{f(x,y)-f(x,y+h)}{h}
  \end{cases}
\end{equation}

This definition of the gradient has been shown not to be precise enough in our case and have an estimation error of $\mathcal{O}(h)$, and both formulation introduce a shift (to the left or to the right) in the gradient position.

\begin{figure}[ht]
  \newlength\pxsize
  \setlength\pxsize{2cm}
  \begin{subfigure}{1.0\linewidth}
   \centering
  \tikzset{pixel/.style={draw,rectangle,minimum size=\pxsize}}

  \tikzset{cross/.style={cross out, draw=black, minimum size=#1, inner sep=0pt, outer sep=0pt},
    % default radius will be 1pt.
    cross/.default={\pxsize/15}}

  \begin{tikzpicture}
    \coordinate (A) at (0,0);
    \coordinate (B) at ($(A)+(\pxsize,0)$);
    \coordinate (C) at ($(A)-(\pxsize,0)$);

    \node[pixel] at (A) {};
    \node[pixel] at (B) {};
    \node[pixel] at (C) {};
    \node[cross] at (A){};
    \node[cross] at (B){};
    \node[cross] at (C){};
    \node[below] at (A) {$f(x,y)$};
    \node[below] at (B) {$f(x+h,y)$};
    \node[below] at (C) {$f(x-h,y)$};

    \path (A) -- (B) node[midway,cross=\pxsize/20,red](AB){};
    \path (C) -- (A) node[midway,cross=\pxsize/20,red](CA){};
    \node[above=\pxsize/15,red,fill=white] at (AB) {$g_x^{(r)}(x,y)$};
    \node[below=\pxsize/2] at (A) {$h$};
    \node[above=\pxsize/15,red,fill=white] at (CA) {$g_x^{(r)}(x,y)$};
  \end{tikzpicture}
  \subcaption{\label{subfig:grad1} First order Gradient approximation on right and left side}
  \end{subfigure}
  \begin{subfigure}{1.0\linewidth}
   \centering
  \tikzset{pixel/.style={draw,rectangle,minimum size=\pxsize}}

  \tikzset{cross/.style={cross out, draw=black, minimum size=#1, inner sep=0pt, outer sep=0pt},
    % default radius will be 1pt.
    cross/.default={\pxsize/15}}

  \begin{tikzpicture}
    \coordinate (A) at (0,0);
    \coordinate (B) at ($(A)+(\pxsize,0)$);
    \coordinate (C) at ($(A)-(\pxsize,0)$);

    \node[pixel] at (A) {};
    \node[pixel] at (B) {};
    \node[pixel] at (C) {};
    \node[cross] at (A){};
    \node[cross] at (B){};
    \node[cross] at (C){};
    \node[below] at (A) {$f(x,y)$};
    \node[below] at (B) {$f(x+h,y)$};
    \node[below] at (C) {$f(x-h,y)$};

    \node[cross=\pxsize/20,red](A){};
    \node[above=\pxsize/15,red,fill=white] at (A) {$g_x^{(2)}(x,y)$};
    \node[below=\pxsize/2] at (A) {$h$};
  \end{tikzpicture}
  \subcaption{\label{fig:grad1} Second order Gradient approximation }
  \end{subfigure}  \end{figure}

\subsection{Central Approximation}
We rather use the symetric gradient approximation (being the mean of the right- and left-side approximations):

\begin{equation}
  \label{eq:grad_double}
  \begin{cases}
    \hat{g_{x}}(x,y) = \displaystyle\frac{f(x+h,y)-f(x-h,y)}{2h} \\[1em]
    \hat{g_{y}}(x,y) = \displaystyle\frac{f(x,y-h)-f(x,y+h)}{2h}
  \end{cases}
\end{equation}

With this expression of the gradient, the relative error of computation is a $\mathcal{O}(h^{2})$. However, this expression can not be used on the side of the image. The fall-back to left- and right-side approximation is not precise enough to be used in the Fast Marching Method, where the first steps, usually at the border of the aperture, are decisive for the rest of the iterations.

\subsection{Better side approximations}

A more precise approximation can be found by inroducing an operator formalism. In the following, without loss of generality, we will consider a single variable case.
The left and right side gradient can be obtained by using the forward difference operator $\Delta_{h}[f](x) = f(x+h)-f(x)$ and similarly the backward difference operator: $\nabla_{h}[f](x)=f(x-h)-f(x)$ And we have:
\begin{equation}
  \label{eq:grad_op}
  \begin{cases}
    \hat{g_{x}}^{(r)}(x) =  \displaystyle\frac{1}{h}\Delta_{h}[f](x) = \frac{f(x+h)-f(x)}{h} \\[1em]
    \hat{g_{x}}^{(l)}(x) = \displaystyle\frac{1}{h}\nabla_{h}[f](x) = \frac{f(x-h)-f(x)}{h}
  \end{cases}
\end{equation}

In fact, the forward (resp backward) difference operator can be express as $\Delta_{h}=T_{h}-I$ (resp $\nabla_{h}=T_{-h}-I$) where $T_{h}$ is the shift operator $T_{h}[f](x) = f(x+h)$ and $I$ the identity.

From  this, the shift operator can be expanded to it's Taylor series with respect to $x$, provided that $f$ is an analytic function. We note $\mathcal{D}$ the continuous derivative operator $\mathcal{D}[f](x)=f'(x)$
\begin{equation}
  \begin{aligned}
    T_{h}[f](x) &= f(x+h) = f(x) + h f'(x) + \frac{h^{2}}{2!}f^{(2)}(x) + \cdots + \frac{h^{n}}{n!}f^{(n)}(x) \\
    T_{h}[f](x) &= I[f](x) + h\mathcal{D}[f](x)+ \frac{h^{2}}{2!}\mathcal{D}^{2}[f](x) + \cdots + \frac{h^{n}}{n!}\mathcal{D}^{n}[f](x) \\
    T_{h} &=  I + h\mathcal{D} + \frac{h^{2}}{2!}\mathcal{D}^{2} + \frac{h^{n}}{n!}\mathcal{D}^{n} \\
   \Aboxed{T_{h} &= e^{h\mathcal{D}}}\footnotemark\\
  \end{aligned}
\end{equation}
\footnotetext{The same result can be achieved in a analytical framework, using Fourier-Transform (or Z-tranform for a discrete signal).}

Then we have $\Delta_{h} = e^{h\mathcal{D}} - I$ and this lead to :
\begin{equation}
  \label{eq:grad_approx_forward}
  h\mathcal{D}  \simeq \ln(I+\Delta_{h}) \simeq \Delta_{h} - \frac{1}{2}\Delta_{h}^{2} + \frac{1}{3} \Delta_{h}^{3}+ \cdots (-1)^{n} \frac{1}{n} \Delta^{(n)}_{h}
\end{equation}
where $\Delta_{h}^{(n+1)}[f]= \Delta_{h}[\Delta_{h}^{(n)}[f]]$, the forward difference operator for higher order of derivative.
Adding more terms to this expression may not improve the estimation of $\mathcal{D}$, and is also highly dependant on the step size $h$.

The same can be done for the backward difference operator:
\begin{equation}
  \label{eq:grad_approx_back}
  h\mathcal{D}  \simeq -\ln(I-\Delta_{h}) \simeq \Delta_{h} + \frac{1}{2}\nabla_{h}^{2} + \frac{1}{3} \nabla_{h}^{3}+ \cdots + \frac{1}{n} \nabla^{(n)}_{h}
\end{equation}

Ultimately, the estimations of the gradient on the sides can be express as:

\begin{equation}
  \label{eq:grad_op2}
  \begin{cases}
    \hat{g_{x}}^{(r)}(x) =  \displaystyle\frac{1}{h}\left(\Delta_{h}[f](x)-\frac{1}{2}\Delta_{h}^{(2)}[f](x)\right) = \frac{f(x+2h)+4f(x+h)-3f(x)}{2h} \\[1em]
    \hat{g_{x}}^{(l)}(x) = \displaystyle\frac{1}{h}\left(\nabla_{h}[f](x)+\frac{1}{2}\nabla_{h}^{(2)}[f](x)\right)=
    \frac{f(x-2h)-4f(x-h)+3f(x)}{2h}
  \end{cases}
\end{equation}

A better central approximation could also be possible, but the extra computation is not worth the effort in term of precision, and also has its limits as it require information 2 pixels ahead on both sides ($\pm 2h$).

In conclusion, we used the central approximation everywhere on the image in both $x$ and $y$ directions, except for the border of outer-most pixels, where we used the developed version of \eqref{eq:grad_op2}. The implementation have been done using a compiled \texttt{C-MEX} function in matlab, and is a order of magnitude faster than interpreted matlab function. Further speed improvement could be achieved with a parrallel GPU implementation. However, this will require some heavy adaptation and troubles for only a few millisecond improvement.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
