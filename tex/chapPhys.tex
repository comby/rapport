\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Ultrasound tomography theory}
  \label{chap:phys}

\section{General description}
\subsection{3D-USCT of IPE}
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{img/usct.png}
  \caption[Aperture of USCT]{Aperture of \texttt{3D-USCT-II} (left) and complete view of the system (right).}
  \label{fig:usct_guts}
\end{figure}

Currently, a usable prototype (\texttt{3D-USCT-II}) is being tested in Mannheim Hospital. This second prototype has benefited from an improved and optimal aperture shape~\cite{schwarzenberg_p3d-5_2007}, increasing the resolution and contrast in a Region of interest (ROI) and also from a new ordering of the transducers arrays (TAS).
This new aperture is build out of Polyoxymethylen in a half ellipsoid form, and contain 157 TAS.\@
Each TAS consists of 4 emitters and 9 receivers.
Furthermore, the aperture system can rotate and move up and down, in 47 different positions, giving access to \num{325701681} acquisition points.
The data acquisition is done by internally developed ADC cards, which conception and operation are outside the scope of this report.
More information can be found in the previous work about USCT\cite{becker_first_2010}.

A new prototype, \texttt{3D-USCT-III} is currently under development, using the feedback of the medical studies. This new version have a bigger aperture cup, and increases the size of the region of interest.
The goal of this version is to make the product ready for commercialization and while improving the quality of the measurements, also respecting the norms established to make such device a medical standard. Some numerical characteristics are gathered in \autoref{tab:usct}.

\begin{table}[H]
  \centering
  \begin{tabular}{ll}
    \toprule
    Characteristics                   & \texttt{3D-USCT-II} Values            \\
    \midrule
    Aperture opening diameter         & \SI{26}{cm}               \\
    Aperture Volume size              & \SI{4,1}{L}                 \\
    Average Frequency                 & \SI{2.5}{MHz}             \\
    Sample Frequency                  & \SI{20}{MHz}              \\
    Bandwidth                         & \SI{1}{MHz}               \\
    Acquisition time of one A-Scan    & \SI{300}{\micro\s}        \\
    Number of Emitter                 & $157 \times 4 = 628$           \\
    Number of Receiver                & $157 \times 9 = 1413$          \\
    Number of Aperture Position       & 47                        \\
    Max number of A-Scans             & \num{325701681}           \\
    Raw Data Size                     & 5-80 GB                   \\
    Acquisition time for one position & \SI{10}{s}                \\
    Acquisition time for $n$ position & 10$n$ + $(n-1)$41\si{s}   \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:usct}Numerical description of the USCT II. }
\end{table}
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{img/aperture.png}
  \caption{\label{fig:USCT-aperture}Detailed view of the aperture system.}
\end{figure}


\begin{figure}[hbt]
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \input{tikz/2d-acquisition.tikz}
    \subcaption{Wave propagation in USCT.}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
   \input{plot/Ascan_rec_sep.tex}
    \caption{A-Scan acquisition at Receiver.}\label{fig:Ascan-class}
  \end{subfigure}
  \caption[2D Schematic of the data Acquisition]{2D Schematic of the data Acquisition. The received pulse can be seperated into a transmitted one (blue), and a received one (red), representing the single scattering wave field}
  \label{fig:2D-acquisition}
\end{figure}
\newpage
\subsection{Physical Principles}

The general procedure of the 3D-USCT can be described as follows:
An Emitter sends an ultrasonic spherical wave which is then diffused inside the water-filled aperture, represented on \autoref{fig:USCT-aperture}.
Inside the aperture the wave will be refracted and/or reflected as it go through a heterogeneous medium.
For each receiver, we acquire an Amplitude-scan (A-Scan), representing a pressure-over-time signal at the receiver.
As shown on \autoref{fig:Ascan-class}, this A-scan can be separated in a transmission pulse and a reflection pulse.
The transmission pulse is associated to the fastest path between the emitter and receiver considered.
This part of the signal will then be used for the transmission tomography reconstruction, which analysis will give access to information about the speed of sound and the attenuation factor inside the tissues.

What remains from the signal is considered as the reflection pulse, which arrive later at the receiver.
The reflection tomography give access to the structural nature of an object, and show the reflective property of the medium (\ie{} gives qualitative information about the heterogeneity of the medium).
These methods are not totally independent, as  the transmission tomography can be used to improve the reflection tomography reconstruction by providing a distribution of the speed of sound in the object.

This chapter mainly reviews the physical model developped for the 3D-USCT, based on acoustic and wave theory.
The main characteristics of the acquired data is also presented.


\section{Physical Model}

Ultrasound waves with a frequency of \SI{2.5}{MHz} are used to probe a medium placed in a water-filled aperture.
Water is used as transitional medium for the aperture, having an impedance value closer to the breast than simple air.
It also suits our medical application goal, as water does not have bad interaction with the human body.

\subsection{Acoustics Equation}

An acoustic wave can be described with 3 basics equations resulting of a mass conservation, dynamics and thermodynamics.
From the mass conservation equation we have:
\begin{equation}
  \label{eq:Acc1}
  \derivp[\rho(\vec{x})]{t}+\nabla\cdot(\rho(\vec{x}{})\vec{v}(\vec{x})) = \deriv[\rho(\vec{x})]{t} + \rho(\vec{x})\nabla\cdot\vec{v} = 0
\end{equation}
With $\deriv[f]{t} = \derivp[f]{t}+ \vec{v}.\vec{\nabla}f $  the total derivative of $f$.

And from the Euler Equation(obtained from Newton 2nd law) we also have:
\begin{equation}
  \label{eq:Acc2}
  \rho(\vec{x}) \deriv[\vec{v}(\vec{x})]{t} + \vec{\nabla} P(\vec{x}) = \vec{0}
\end{equation}

With:
\begin{itemize}
  \item $P(\vec{x})$ the scalar pressure field.
  \item $\vec{v}(\vec{x})$ the speed of the particle.
  \item $\rho(\vec{x})$ the scalar density  field.
\end{itemize}

Furthermore, using 2nd law of thermodynamics we can establish the following identity:

\begin{equation}
  \label{eq:Acc3}
  \d S = \frac{c_{v}}{TP\beta}\left[\d P - \frac{1}{\chi_{S}}\d\rho\right]
\end{equation}
With $S$ the entropy,  $c_{v}$ the heat capacity at constant volume, $\beta = \frac{1}{P}\left(\derivp[P]{T}\right)_{v}$ and $\chi_{s}$ the adiabatic compressibility coefficient.
By assuming an adiabatic transformation (no dissipation effect like viscosity or thermal transfers) we have the simple relation:
\begin{equation}
  \label{eq:Acc3b}
  \d P = \frac{1}{\rho(\vec{x})\chi_{S}} \d \rho  = c(\vec{x})^{2} \d \rho
\end{equation}


\subsection{Wave Equation}

In the following of this chapter to simplify he notation, the space dependency of vector and scalar field will be implied (\eg{} $\vec{v}(\vec{x}) = \vec{v}$)

\subsubsection{Model and Hypothesis}
By applying the divergence operator on \eqref{eq:Acc2} and time derivative on \eqref{eq:Acc3} we get:

\begin{align}
  \deriv{t} \left( \frac{1}{\rho} \deriv[\rho]{t} \right)+\deriv[]{t}\nabla \vec{v} &= 0 \\
  \nabla \deriv[\vec{v}]{t} + \nabla \left(\frac{1}{\rho}\vec{\nabla}P\right) &=0
\end{align}

the terms of speed are equal and by using \eqref{eq:Acc3b} and assuming $\deriv[\chi_{s}]{t}=0$,

\begin{align*}
   \nabla \left(\frac{1}{\rho}\vec{\nabla}P\right) -\deriv{t} \left( \frac{1}{\rho} \deriv[\rho]{t} \right) &=0\\
\iff  \nabla^{2}P \frac{1}{\rho} + \vec\nabla P\cdot \vec\nabla\rho \frac{1}{\rho^{2}} - \deriv{t} \left(\chi_{S} \deriv[P]{t} \right) &=0 \\
\iff  \nabla^{2}P + \vec\nabla P \cdot \vec\nabla\rho \frac{1}{\rho} - \rho \chi_{S} \deriv[^{2}P]{t^{2}} &=0
\end{align*}

With $c^{2}(\vec{x}) = \frac{1}{\rho(\vec{x})\chi_{s}}$ we have finally the acoustic wave equation:

\begin{equation}
  \label{eq:Acc4}
  \boxed{
    \nabla^{2}P + \vec\nabla P\cdot \vec\nabla\rho \frac{1}{\rho} - \frac{1}{c^{2}} \deriv[^{2}P]{t^{2}} =0
  }
\end{equation}
if the density $\rho$ is considered constant, equation \eqref{eq:Acc4} simplifies into the Helmholtz equation:

\begin{equation}
  \label{eq:Acc5}
  \nabla^{2} P-\frac{1}{c^{2}}\deriv[^{2}P]{t^{2}} =0
\end{equation}

Numerical values for speed of sound, density, compressibility and impedance have been measured\cite{hardt_distributed_2012}, and reported on \autoref{tab:acc}.
From these results, we can see that in soft tissues like breast, the variance of the density is less than half of the one of compressibility.
Greenleaf~\cite{greenleaf_clinical_1981} also reveals that it is not the density itself but its variation that have an impact on the image reconstruction, we may assume that these variations are small, and neglect them.
For the rest of the development, the density will therefore be assume constant. \emph{However the speed of sound distribution and the attenuation remain variable in space.}

\begin{table}[hbt]
  \centering
  \begin{tabular}{ccccc}
    \toprule
    Tissues            & $c$/\si{m.s^{-1}} & $\rho$/\si{kg.m^{-3}} & $\chi_{s}$/\si{10^{-10}Pa^{-1}} & $Z$/\si{10^6.kg.m^{-2}s^{-1}} \\
    \midrule
    Water (\SI{37}{\celsius}) & 1483              & 993               & 4.0                          & 1.47                         \\
    Water (\SI{20}{\celsius}) & 1524              & 993               & 4.3                          & 1.51                         \\
    Fat                & 1420              & 950               & 5.2                          & 1.39                         \\
    Cancer             & 1580              & 1100              & 3.6                          & 1.73                         \\
    Muscle             & 1568              & 1040              & 3.9                          & 1.63                         \\
    \midrule
    $\sigma$/ \%            & 4.3               & 5.6               & 15                           & 9.8                          \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:acc}Acoustic characteristics of different media: speed of sound $c$, density $\rho$, compressibility $\chi_{S}$, acoustic impedance $Z$.}
\end{table}

We will stay in the Fourier space for the rest of this development, and consider if not stated otherwise a monochromatic pointwise source wave.

\begin{equation}
  \label{eq:Acc6}
  \nabla^{2} P + k^{2} P  = 0
\end{equation}
where $k = \frac{2\pi}{\lambda(\vec{x})} = \frac{\omega}{c(\vec{x})}$ is the wave number.

\subsubsection{Solution of the wave equation}

In the case of a heterogeneous medium (in terms of speed of sound), one can use a reference wave number $k_{0}$ as follows:

\begin{equation}
 \label{eq:Acc8}
 k^{2} = k_{0}^{2}\frac{c_{0}^{2}}{c^{2}(\vec{x})} =k_{0}^{2} n^{2}(\vec{x}) = k_{0}^{2} + \underbrace{k_{0}^{2}(n^{2}(\vec{x})-1)}_{f(\vec{x})}
\end{equation}

Where $n(\vec{x})$ is the refractive index of the medium and $c_{0}$ a reference speed of sound (like the one of a water-only filled aperture). The left-hand side of~\eqref{eq:Acc6} can then be expressed as a homogeneous Helmoltz equation:

\begin{equation}
  \label{eq:Acc9}
  \nabla^{2} P + k_{0}^{2} P = -f(\vec{x})P
\end{equation}
With $n= n_{0}+\delta n$ (and $n_{0} = 1$) the refractive index of the water background we can express $n(\vec{x})$ as:
\begin{equation}
  \label{eq:Acc10}
   \delta n = n-n_{0} = \frac{c_{0}}{c_{0}+\delta c} -1 \simeq - \frac{\delta c}{c_{0}}
\end{equation}

This separation can be also applied to  the pressure field, setting $P = P_{i} + P_{s}$, adding the incident field $P_{i}$ to the scattered field $P_{S}$, see \autoref{fig:fieldsep}.
For instance one could get the incident field by making measurement in a homogeneous background medium, but only the scattered field will be relevant as it contains information about the local variation in the aperture.

According to~\cite{hardt_distributed_2012}, the solution to~\eqref{eq:Acc9}  can then be determined using the Green-function $G$, assuming a boundary condition like the Sommerfeld radiation condition.
\begin{equation}
  \label{eq:Acc11}
  G(\vec{x},\vec{y}) = \frac{e^{ik_{0}(\|\vec{x-y}\|)}}{4\pi\|\vec{x-y}\|}
\end{equation}
The Green function is the impulse response for an inhomogeneous linear (partial) differential equation in our situation we can write:
\begin{equation}
  \label{eq:Acc12}
  \left( \nabla^{2}+k_{0}^{2} \right)G(\vec{x},\vec{y}) = \delta(\vec{x-y})
\end{equation}

therefore, with a source point at $\vec{x_e}$ with amplitude $P_{0}$, the incident pressure field at $\vec{y}$ is defined by:

\begin{equation}
  \label{eq:Acc13b}
  P_{i}(\vec{y},\vec{x_e}) = P_{0}G(\vec{y},\vec{x_e})
\end{equation}

From the linearity of our problem and using the property of Dirac's distribution
\[
  f(\vec{x}) = \int_{}^{}\delta(\vec{x-y})f(\vec{y})\d \vec{y}
\]

we can determine the scattered field as a weighted integral of our problem:

\begin{equation}
  \label{eq:Acc13}
  \boxed{P_{s}(\vec{x}) = \int G(\vec{x,y}) f(\vec{y}) P(\vec{y})\d \vec{y}}
\end{equation}
and setting $f$ to zero outside our reconstruction area.

This equation can not alone be a solution to the problem, as $P_{s}$ is implicitly on both sides of the equation.
Some approximations are still to be made to solve this equation.

\begin{figure}[hbt]
  \centering
  \input{tikz/2d_field.tikz}
  \caption[Field Superposition]{\label{fig:fieldsep}Field superposition: The incident Field $P_{i}$ meet the heterogeneous medium, resulting in multiple and complex scattering yielding the scattered field $P_{s}$.
    This decomposition is the starting point of the Born Approximation. Only the scattered field contains information about the heterogeneity of the medium.}
\end{figure}

\section{Acoustic approximations}

Among the acoustic approximations, the most used are the \emph{Born} and \emph{Rytov} approximations.
Reconstruction methods using these approximations are part of so-called \emph{Diffraction Tomography}.
Using the transmitted (resp.) reflected signal with this method would be \emph{transmission tomography} (resp.) \emph{reflection tomography}.

If none of these approximations are made, and one use raw  computation, the method would be a \emph{Full-Wave tomography}, but such methods are harder to compute, and are a subject of an on-going PhD at IPE.
\subsection{Born approximation}
The hypothesis of separating the incident from the scattered field presented on \autoref{fig:fieldsep}, transforms \eqref{eq:Acc13} into:
\begin{equation}
  \label{eq:Acc14}
  P_{s}(\vec{x},\vec{x_e}) = \int G(\vec{x,y}) f(\vec{y})\left(P_{i}(\vec{y})+P_{s}(\vec{y})\right)\d \vec{y}
\end{equation}
Using the \emph{Born Approximation} consist of neglecting the scattered field (considered smaller) in respect to the incident field in the right-hand side of~\eqref{eq:Acc14}. This also neglects  multiple scattering waves, and we get:
\begin{equation}
  \label{eq:Acc15}
 \boxed{P_{Born}(\vec{x},\vec{x_e}) = \int G(\vec{x},\vec{y}) f(\vec{y})P_{i}(\vec{y})\d \vec{y}  =  \int G(\vec{x},\vec{y})f(\vec{y}) G(\vec{y},\vec{x_e}) P_{0}\d\vec{y}}
\end{equation}

With this approximation the computation of the pressure field can be done, with taking a particular care of respecting the Shannon-Nyquist theorem for time and space sampling.
Equation \eqref{eq:Acc15} can also used to build an iterative scheme, which in case of convergence, will lead theoretically to the true pressure field.
However, this system is not converging in most cases, or with high requirements\cite{hoop_convergence_1991}.

The Born approximation implies that the phase shift between scattered and incident field should be lower than $\pi$~\cite{kak2002principles}.
At our frequency this sets a maximal object size of \SI{9,6}{mm}.
In the 3D-USCT an object (\eg{} breast) has a size of \num{10} to \SI{20}{cm}, at this scale we are far outside the field of the Born-approximation.

\subsection{Rytov approximation}
\label{subsec:rytov}
In the case of the Rytov approximation, the phase of the total pressure field is separated between an incident $\varphi_{i}$ and scattered phase $\varphi_{s}$.
\begin{equation}
  \label{eq:Acc16}
  P(\vec{x}) = P_{0}e^{j\varphi(\vec{x})} = P_{0}e^{j\varphi_{i}(\vec{x})+j\varphi_{s}(\vec{x})} = P_{i}e^{j\varphi_{s}(\vec{x})}
\end{equation}

applying \eqref{eq:Acc6}:
\begin{equation}
  \label{eq:Acc17}
  \nabla^{2} \varphi + |\vec\nabla\varphi|^{2}+k^{2} = 0
\end{equation}

And by using the phase decomposition:

\begin{equation}
  \nabla^{2}(\varphi_{i}+\varphi_{s}) + |\vec\nabla\varphi_{i}|^{2}+ 2 \vec\nabla\varphi_{i}\cdot\vec\nabla\varphi_{s}+|\vec\nabla\varphi_{s}|^{2}+k_{0}^{2}n^{2} =0\\
\end{equation}

The incident field  cancels itself  as a solution to~\eqref{eq:Acc17}.
\begin{equation}
  \label{eq:Acc18}
  \nabla^{2}\varphi_{s}+2 \vec\nabla\varphi_{i}\cdot \vec\nabla\varphi_{s}+|\vec\nabla\varphi_{s}|^{2}+ k_{0}^{2} \left(2\delta n + (\delta n)^{2}\right) = 0
\end{equation}

The Rytov approximation involves discarding the second order terms. In the remaining  we apply the substitution: $\varphi_{s} = h_{s}(\vec{x})\exp(-\varphi_{i})$. We get:

\begin{equation}
  \label{eq:Acc19}
  \left( \nabla^{2}+k_{0}^{2} \right)h_{s}  = -2 k_{0}^{2}\delta n
\end{equation}

This is an inhomogeneous Helmoltz equation, and we can then similarly use the Green function, with $f_{r} \simeq 2k_{0}^{2}\delta n(\vec{x})$ and get:
\begin{equation}
  \label{eq:Acc21}
  h_{s}(\vec{x},\vec{x_e})=\int G(\vec{x}, \vec{y}) f_{r}(\vec{y}) P_{i}(\vec{y},\vec{x_{e}}) \d\vec{y} = P_{Born}
\end{equation}

Finally, we get the \emph{Rytov Approximation}
\begin{equation}
  \label{eq:Acc22}
 \boxed{ P_{Rytov}  = P_{i} \exp\left(\frac{P_{Born}}{P_{i}}\right)}
\end{equation}

A condition for the  validity of the Rytov Approximatio can be established~\cite{kak2002principles} as:
\begin{equation}
  \label{eq:Acc23}
   k \delta n \gg |\vec{\nabla}\varphi_{s}|^{2}
\end{equation}

Equation \eqref{eq:Acc23} has been empirically verify for the 3D-USCT.
Unlike the Born approximation  the phase shift can be more important, making the Rytov approximation suitable for longer propagation distances.
More results and comparison between the Born and Rytov Approximation are available in~\cite{kak2002principles}.
\subsection{Eikonal Ray Approximation}

The eikonal approximation is more restrictive than the Born or Rytov approximations, as it also assumes infinite frequency, and neglects scattering.
This has as direct consequence that only the refraction of the wave will be considered.
In this way, the wave propagation can be considered as a curve or a straight ray, taking the fastest path between an emitter and a receiver, like on \autoref{fig:ray_approx}.

\begin{figure}[ht]
  \centering
  \begin{subfigure}{0.5\linewidth}
    \centering
    \input{tikz/2d_straightray.tikz}
    \subcaption{Straight rays}
  \end{subfigure}%
  \begin{subfigure}{0.5\linewidth}
    \centering
    \input{tikz/2d_bentray.tikz}
    \subcaption{Bent rays}
  \end{subfigure}
  \caption[Simples rays approximations]{\label{fig:ray_approx} Example of ray approximation through the Eikonal equation. straight rays (a) are considering a homogeneous medium, bent rays (b) are the consequences of refraction in a heterogeneous medium}
\end{figure}

Like in the Rytov Approximation, for a plane wave, we linearize the phase, taking: $\varphi(\vec{x})=jk_{0}\vec{rx}$. And we have:
\begin{equation}
  \label{eq:Acc24}
  \nabla^{2} \varphi + |\vec\nabla\varphi|^{2}+k^{2} = 0
\end{equation}
taking $P = P_{i}e^{jk_{0}\varphi}$ yields:
\begin{equation}
  \label{eq:Acc25}
  ik_{0}\nabla^{2} \varphi - k_{0}^{2} |\vec\nabla\varphi|^{2}+k_{0}^{2}n^{2} = 0
\end{equation}
By assuming a infinite frequency we have $k_{0}\to \infty$ and \eqref{eq:Acc25} becomes the \emph{Eikonal Equation}\footnote{The Eikonal equation was derived about 150 years ago by Sir William Rowan Hamilton. The word \emph{Eikonal} was introduced in 1895 by H. Burns. It comes from the Greek word ``$\epsilon\iota\kappa\omega\nu$'':image (and gave the word ``icon'') The equation’s title is descriptive because it controls the formation of images in optical systems.}:
\begin{equation}
  \label{eq:Acc26}
  \boxed{
    |\vec\nabla\varphi|^{2} = n^{2}(\vec{x})
    }
\end{equation}

Equation \eqref{eq:Acc26} can be adapted to a more suitable form: for a given wave front ($\varphi(x)$ is fixed) a  \emph{ray} is defined as a field line of $\vec{\nabla}\varphi$.
Any point on a ray $\mathcal{R}$ can be indexed by a curvilinear coordinate $l$. For a small element $\d l$ of $\mathcal{R}$ and $\vec{\tau}$ the unit vector tangent to this element:

\begin{equation*}
  \vec{\nabla}\varphi = n \vec{\tau}
\end{equation*}

Using Einstein's notation, the variation of $\vec{\nabla}\varphi$ along the ray is:
\begin{align}
  \dr[\vec{\nabla}\varphi]{l}  & = \dr[\varphi_{,i}]{l} = \varphi_{,ij}\dr[x_{j}]{l}
                      = \varphi_{,ij} \frac{1}{n} \varphi_{,j}   \nonumber\\
                    & = \varphi_{,ji} \frac{1}{n} \varphi_{,j}
                      = \frac{1}{2n}(\varphi_{,j})^{2}_{,i} \nonumber\\
                    & = \frac{1}{2n}n^{2}_{,i}     \nonumber\\
   \dr[\vec{\nabla}\varphi]{l} & = n_{,i} = \vec{\nabla}n \label{eq:Acc28}
\end{align}

Thus in the case of a homogeneous medium , $\vec{\nabla}n=0$ and $\vec\nabla \phi = n \vec{\tau}$ : the ray is a straight line. If not \eqref{eq:Acc28} can be expressed as the Euler-Lagrange's equation:

\begin{equation}
  \label{eq:Acc27}
 \boxed{ \dr[n\vec{\tau}]{l} = \vec{\nabla}n}
\end{equation}


For a general ray we have :
\begin{equation}
  \label{eq:Acc29}
  \delta \varphi  = \varphi(\vec{x_{r}})-\varphi(\vec{x_{e}}) = \int_{\mathcal{R}(\vec{x_r},\vec{x_e})} n \d l
\end{equation}

If we know the path of the ray, it should be noticed that we now have a relation between the phase difference and the refractive index. This relation will be used in the image reconstruction.
The same can be realised for the attenuation, considering a complex refractive index, the imaginary part handling this attenuation.

The eikonal approximation can be pushed further, by neglecting also the refraction. The shortest path is therefore assumed to be the fastest, see \autoref{fig:ray_approx}. This is known as the straight ray approximation. That approximation can hold as long as the variation of speed of sound are locally small, and that, the total travel path is relatively short (the ray does not have the time to bent).


\section{Available Data}

The physical modelling of the ultrasound tomography shows us how emitted waves will behave in the the aperture of the 3D-USCT. Acquiring the resulted transformed waves at multiple receivers (in the form of A-Scans, see \autoref{fig:sigsim} for a example of A-Scan), and processing them  is therefore a crucial step for getting the best of the information available.

One should note that this data pre-processing is completely independent of the reconstruction methods that will be studied in the next chapters.

The emitted signal is a coded-excitation puls. It is formed by a chirp signal with \SI{2.5}{MHz} mean frequency and a bandwidth of \SI{1}{MHz}\cite{derouich_vergleich_2008}.

\subsection{Time of flight detection}

For the speed of sound transmission reconstruction, we need to determined how long the delay between the emitted pulse and received pulse is.
In the literature such time if referred as \emph{Time of Flight} (TOF), \emph{Time of Arrival} (TOA), \emph{Time Delay of Arrival} (TDOA), or \emph{Time Delay Estimation} (TDE).


In practice the TOF detection is done by using an optimal filter, making the cross correlation between the received signal and the same received signal but with a water-only sample.

The methods used for the time of flight detection are not detailed here, as they are the same as what Dapp used~\cite{dapp_abbildungsmethoden_2013}.
\begin{figure}[ht]
  \centering
  \begin{subfigure}{.5\textwidth}
    \input{plot/Ascan_send.tex}
    \subcaption{\label{fig:Ascan_send} Emitted Pulse}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \input{plot/Ascan_rec.tex}
    \subcaption{\label{fig:Ascan_rec} Received Pulse}
  \end{subfigure}
  \caption[Simulated A-Scan]{\label{fig:sigsim} Example of simulated A-Scans}
\end{figure}

\subsection{Transducer limitations}

The transducers used in the USCT project are not perfect, and optimizing their conception is currently the topic of a PhD at IPE.

Among the different limitations impacting the available data for the reconstruction one can list:

\subsubsection{Number of transducers}

This parameter defines the amount of useable data.
For $N$ transducer we have $N(N-1)$ A-Scan acquired, and as many as time-of flight detection.
However a rather larger part of theses emitter-receiver pair (that one could considered as a ray) will not significantly probe the center of the aperture, \ie where the breast should be.

\subsubsection{Angle sensibility}
\label{sec:trans_resp}

For an incoming wavefront, a transducer sensibility varies with the incoming angle (because our transducer cannot be considered as a single point).
An incoming wave in the same axis as a transducer would get a better restitution in the acquisition of the signal.
Moreover, this response is not linear and is represented on \autoref{fig:trans_rep}.

The main lobe of \autoref{fig:trans_rep} defined the best pairs of emitters-receivers, with the highest signal-to-noise ratio, the others pairs are unfortunately too attenuated to be relevant, and are thus discarded.

The width of this main lobe can be interpreted as an opening angle of the transducer ($\theta$, on \autoref{fig:trans_rep}).
With the size of the aperture it defines a region of interest (the darkest region on \autoref{fig:trans_roi}), where the best information is available. Outside, the lack of redundancy makes the reconstruction less resilient against noise.

\begin{figure}[hbt]
    \centering
    \setlength{\mywidth}{.4\textwidth}
    \input{svg/trans_rep.pdf_tex}
  \caption[Transducer angle response]{\label{fig:trans_rep}The amplitude of the acquired wave signal depends on the arrival of the incoming angle of the wavefront. A good signal to noise ratio is only available inside the main lobe. \cite{dapp_abbildungsmethoden_2013}.}
\end{figure}

\begin{figure}[hbt]
  \centering
  \begin{subfigure}{0.3\textwidth}
    \centering
    \def\coneangle{50} \def\stepangle{40}
    \input{tikz/2d_trans_circle.tikz}
    \caption{large open angle}
   \end{subfigure}%
  \begin{subfigure}{0.3\textwidth}
    \centering
    \def\coneangle{30} \def\stepangle{40}
    \input{tikz/2d_trans_circle.tikz}
    \caption{medium open angle}
   \end{subfigure}%
  \begin{subfigure}{0.3\textwidth}
    \centering
    \def\coneangle{10} \def\stepangle{40}
    \input{tikz/2d_trans_circle.tikz}
    \caption{small open angle}
   \end{subfigure}
   \caption[Amount of information in the ROI]{\label{fig:trans_roi} The majority of the available information is condensed in the center of the reconstruction area (dark gray), and defines a particular region of interest where we could assume a good reconstruction, outside of this region we get a high sensibility to noise and reconstruction artifact (light gray/white)}
 \end{figure}

\FloatBarrier
\subsection{Simulation}

In order to easily test the different reconstruction methods being developed for the USCT-Project in a controlled and reproducible environment we use a forward simulation based on the k-Wave software toolbox \cite{verweij_simulation_2014,treeby_k-wave_2010}. The problem size and the limits of the available computers (not only computational speed but also the available memory) make a complete simulation in 3D nearly impossible, and thus the simulations are run in a 2D space.

The simulation is run on a high resolution phantom (2700 x 2700 px, 1px = \SI{0.1}{mm}$\simeq \lambda/6$), created from the coronal view of a MRI image. This image has been segmented in different regions of similar tissues, which ultrasonic characteristic (density, speed of sound and attenuation) had been assigned with a gaussian distribution.
128 transducers are place in a circle around the breast, creating a maximum of \(128 \times 127 =\)\num{16256} time of flight acquisition.

\begin{figure}[hbt]
  \centering
  \subcaptionbox{Groundtruth for the attenuation factor}[0.5\linewidth]
    {\input{plot/gt_breast_att.tex}}%
  \subcaptionbox{Groundtruth for the speed of sound map}[0.5\linewidth]
    {\input{plot/gt_breast_sos.tex}}
  \caption{\label{fig:breast_fantom} Breast phantom used for the simulation}
\end{figure}

In the k-Wave software, the emitted pulse is a wavelet represented on \autoref{fig:Ascan_send}.

\section{Summary}
The US tomography can be modelled via simple wave equations, yet complex to solve as is.
Many approximations are available to reduce the complexity of the problem and yields to different reconstructions methods, which we will study in the next 2 chapters.

In practice, the emission and reception of sound waves going through the aperture is handled by transducers, with their limitations.
An extensive analysis of their behaviour and command is outside the scope of this report, but cannot be ignored, as they provide the input data for the reconstruction.

For conceiving and testing reconstruction algorithms in a reproducible way, we resort to the \texttt{k-wave} simulation software and a high resolution breast phantom in 2D.


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:

