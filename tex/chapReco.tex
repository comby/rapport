\documentclass[main.tex]{subfiles}
\begin{document}
\renewcommand{\m}{\vec{m}}
\chapter{Tomographic reconstruction methods}
\label{chap:reco}

This chapter will be a non exhaustive review of the different algorithms that could solve the previously introduced linear inverse problem

\begin{equation}
  \label{eq:inv_pb}
  \y=\M\x
\end{equation}

With as remainder $\y$ the time of flight vector, determined by $\M$ a $M \times N$ matrix encoding how the $M$ rays (emitter-receiver pairs) are influenced by the slowness vector $\x$ representing $N$ pixels of the image.

Hereafter we denote $\mathcal{X}$ the space of the posssible solutions and $\mathcal{Y}$ the space of the possible acquired data.

\section{Inversion theory}

Inverse Problem have been intensely studied in the mathematical field. In particular \emph{well-posed} problem in the sens of Hadamard have the following properties:
\begin{itemize}
  \item A solution exist $Im(M) = \mathcal{Y}$
  \item The solution is unique $Ker(M)={0}$
  \item The solution always depends continuously on the initial conditions (small changes in the data implies small changes in the solution)
\end{itemize}

The third condition can be described by the condition number of $\M$ defined as follow:

\begin{equation}
  \label{eq:invpb3}
  \xi(\M) = \frac{\sigma_{max}(\M)}{\sigma_{min}(\M)}
\end{equation}
With $\sigma_{max}(\M)$ and $\sigma_{min}(\M)$ the biggest and the smallest singular value of $\M$


Problem \eqref{eq:inv_pb} is both a \emph{ill-posed} problem (it is unlikely to have $N=M$) and \emph{ill-conditioned} problem.

Estimation of the conditions number have been done for various combination of pixel resolution and number of rays, yields in USCT case a condition number of $\xi(\M) > 10^{10}$. A typical good value should be $\xi(\M)<10$.
In other words, it is \emph{very} sensitive to noisy input data.

Solving directly the problem \eqref{eq:inv_pb} if therefore replace with a \emph{surrogate problem} with the following requirements:
\begin{itemize}
  \item Well-posed
  \item The solution of the surrogate problem should belong to $\mathcal{X}$ (consistency).
  \item Computationally feasible.
\end{itemize}
This problem have often the form:
\begin{equation}
  \label{eq:sr_pb}
  \argmin{\x}{f(\x,\y)+g(\x)}
\end{equation}
With $f(\x,\y)$ the data-fidelity term and $g(\x)$ a prior-knowledge term.
Assuming $f$ and $g$ are suitable functions (continuous, convex, possibly differiantiable) and $\mathcal{X}, \mathcal{Y}$ are also convex, the problem will be well-posed.

\section{Bayesian framework}
To compare the various solvers available, let's consider a more realistic problem, where noise is modelled with help of probabilities:

\begin{equation}
  \label{eq:inv_pb_pr}
  \y=\M\x+\vec{n}
\end{equation}

The noise component $\vec{n}$ is considered unknown and is thus a random variable: $\vec{n} \sim p_{\vec{n}}(\vec{n})$.
In particular $\vec{n}$ could be choose as an independant white gaussian noise:
\begin{equation}
  \label{eq:gauss}
  p_{\vec{n}}(\vec{n})=\frac{1}{\sigma \sqrt{2\pi}}\exp\left(-\frac{1}{2\sigma^{2}}\|\vec{n}\|_{2}^{2}\right)
\end{equation}



\subsection{Maximum likelihood}
By introducing probabilities, $\y$ becomes also a random variable, and is an affine transformation of $n$:
\begin{equation}
  \label{eq:likelihood}
  \y \sim p_{\vec{n}}(\y-\vec{Mx}|\x)
\end{equation}
This is also call the \emph{likelihood} of $\y$.
Maximizing this likelihood, with respect to $\x$ will yields the most probable acquired data. This has for underlying hypothesis that the observation made is not a rare event. The Maximum likelihood problem is then:

\begin{equation}
  \label{eq:ML}
  \x_{ML} = \argmax{\x}{p_{\y}(\y)} \iff \argmin{\x}{\|\y-\vec{Mx}\|_{2}^{2}}
\end{equation}
Maximizing the Likelihood is equivalent to minimizing the negative log-likelihood. With a Gaussian noise, the Maximum likelihood approach boils down to a classic least square Problem.

\subsubsection{Least square solution}

This norm minimization problem is then solve (gradient of the solution is null) and give the analytical solution:

\begin{equation}
  \label{eq:ML_sol}
  \x_{ML}= (\M^{T}\M)^{-1}\M^{T}\y
\end{equation}

Moreover, the solution $\x_{ML}$ is a linear combination of the observation data, and can also be understand as a filtering process, from a tomographic point of view this is understood as the well known \emph{filtered backprojection} (often done in X-ray tomography, and executed in the frequency domain for simplicity).

\subsubsection{Correlated Noise}
\label{sec:ML_corr}
If the noise carry also a correlation between its component, a correlation matrix $\vec{\Sigma}$ can be added in the mix (matrix of the covariance between each component of $\vec{n}$). This gives:

\begin{equation}
  \label{eq:gauss_corr}
  p_{\vec{n}}(\vec{n}) = \frac{1}{\sqrt{|\det(2\pi \vec{\Sigma})}}\exp
  \left( -\frac{1}{2}\x^{T}\vec{\Sigma}\x \right)
\end{equation}

This leads to a solution using a \emph{weighted norm}, hence the weighted least square problem:

\begin{equation}
  \label{eq:ML_pond}
  \argmax{\x}{p_{\y}(\y)} \iff \argmin{\x}{ \|\y-\vec{Mx}\|_{\vec{\Sigma}}^{2}}
\end{equation}
where $\|\x\|_{\Sigma}^{2} = \x^{T}\vec{\Sigma}\x$, is the norm weighted by $\vec{\Sigma}$.

\subsection{Maximum \emph{a posteriori}}
In the maximum likelihood approach, few or nothing is assumed about the properties of $\x$. With the maximum \emph{a posteriori}, $\x$  is also considered as a random variable, on which one could assume some statistical characteristics, and models it with a probability density function $p_{\x}(\x)$, called the \emph{prior probability} of $\x$. This added information can then be used in a bayesian framework, where the likelihood is now dependent of a previously assumed value of $\x$, and noted $p_{\y}(\y|\x)$. Using the \emph{Bayes Rule}, it gives access to $p_{\x}(\x|\y)$:

\begin{equation}
  \label{eq:Bayes}
  p_{\x}(\x|\y) = \frac{p_{\x}(\y|\x)p_{\x}(\x)}{p_{\y}(\y)}
\end{equation}
This quantity is the \emph{posteriori} probability of $\x$. In other words, what is the probability of $\x$ to yields the measured output data $\y$ ? The most probable value is then the Maximum a posteriori.

The solution to the \emph{Maximum a posteriori} is then:
\begin{equation}
  \label{eq:MAP}
  \x_{MAP} = \argmax{\x}{p_{\x}(\x|\y)} = \argmin{\x}{ -\log(p_{\y}(\y|\x))-\log(p_{\x}(\x))}
\end{equation}

Again, the negative log-likelihood is considered, and the term $p_{\y}(\y)$ is discarded, as it does not depends on $\x$.

For example, if $\x$ is assumed to be normally distributed:
\begin{equation}
  \label{eq:MAP_gauss}
  \x_{MAP} = \argmin{\x}{\frac{1}{2\sigma_{n}^{2}} \|\y-\vec{Mx}\|_{2}^{2}+ \frac{1}{2\sigma_{x}^{2}}\|\x\|_{2}^{2}}
\end{equation}

In the same way, if $\x$ follow a Laplace law $p_{\x}(\x) = \frac{\lambda}{2}\exp(-\lambda\|\x\|_{1})$ :

\begin{equation}
  \label{eq:MAP_gauss2}
  \x_{MAP} = \argmin{\x}{\frac{1}{2\sigma_{n}^{2}} \|\y-\vec{Mx}\|_{2}^{2}+ \lambda\|\x\|_{1}}
\end{equation}

\section{Iterative reconstruction}

In the case of the least square problem, the analytical expression of the  solution is $\x_{ML}= (\M^{T}\M)^{-1}\M^{T}\y$; However, it is not feasible to compute this expression directly.
The matrix $\M^{T}\M$ is too huge to be stored in memory, and even if it was sparse, its inverse is not.
Therefore, an iterative reconstruction is necessary.

There exists many different iteratives algorithms with various names (ART, SART, POCS, EM, CGLS, \etc{}...). Some of them are mostly the same and tweaks to be interpreted as an other one. In such a wide field to explore and test for the USCT project, this report will focused on the Algebraic Reconstruction Technique (ART) familly of algorithms.


\subsection{Algebraic Reconstruction Technique}

The first Algebraic Reconstruction Technique (ART) have been introduced by Kaczmarz \cite{kaczmarz1937angenaherte} and develop by Gordon \cite{gordon_algebraic_1970}. The following  explanation is based on \cite{andersen_simultaneous_1984}
This method directly try to solve the inverse problem $\y=\M\x$, without any assumption on the noise.
To explain the solving processus of ART let's rewrite \eqref{eq:inv_pb} as:
\begin{equation}
  \label{eq:sys_prob}
  \left\{
  \begin{aligned}
    m_{11}x_{1} +  m_{12}x_{2} &+ \cdots &+ m_{1N}x_{N} &= y_{1} \\
    m_{21}x_{1} +  m_{22}x_{2} &+ \cdots &+ m_{2N}x_{N} &= y_{2} \\
    \vdots                       &    &             &\vdots      \\
    m_{N1}x_{1} +  m_{N2}x_{2} &+ \cdots &+ m_{NN}x_{N} &= y_{N} \\
  \end{aligned}\right.
\end{equation}

The image to reconstruct can be considered as a single point in a $N$-dimensional space. Each equation of \eqref{eq:sys_prob} is thus a hyperplan\footnote{NB: the terms hyperplans and equations are use synonymously} in the image space.

Knowing an estimate  $\x^{(k)}$of $\x$ (from an initial Guess or a previous reconstruction) one can determine a new estimate by projecting successively on the $i_{k}$-th row of $\M$, giving a intermediate estimation $\x^{(k)}$.
With the initialisation: $\x^{(k,0)} = x^{(k-1)}$, the process can then be described as follow:
\begin{equation}
  \label{eq:art}
  \boxed{
    \x^{(k)} = \x^{(k)} + \frac{\m_{i_{k}}}{\|\m_{i_{k}}\|^{2}_{2}}\left(y_{i_{k}}- \x^{(k)} \cdot \m_{i_{k}}\right)
  }
\end{equation}
The index of row $i_{k}$ is made dependent of the iteration.
The naive approach would be to use the natural indexing $i_{k} = k\mod M$, by cycling on the rows of M.
The choice of the indexing method can greatly influence the convergence speed of the reconstruction.

\subsection{Simultaneous Algebraic Reconstruction Technique}

The Simultaneous Algebraic Reconstruction Technique (SART) unlike ART does not update $\x$ at each row iteration, and its update rule can therefore easily be parallelized.
In the Literature the SART algorithm is sometimes also call SIRT (especially in the CT community), where SIRT denote in fact a wider family of algorithms, based on Landweber methods \cite{landweber_iteration_1951} and can also be understood as Richardson’s iteration scheme\cite{saad_iterative_2000}.
The \texttt{AIRToolBox} \cite{hansen_air_2012} provides implementations of this algorithms.

The SART update scheme can be summarize in the following equation:

\begin{equation}
  \label{eq:SART}
  \x^{(k+1)} = \x^{(k)}+ \vec{C}\M^{T}\vec{R}(\y-\M\x^{(k)})
\end{equation}
With $\vec{C}$ and $\vec{R}$ diagonal matrices use for the ponderation:
\begin{equation}
  \label{eq:SART_CR}
  \begin{cases}
    c_{jj} = \left( \sum_{i=1}^{M}|M_{i,j}|\right)^{-1} \\

    r_{jj} = \left( \sum_{j=1}^{N}|M_{i,j}|\right)^{-1}
  \end{cases}
\end{equation}
Furthermore, these expressions explicitly declare that none of the rows are empty (every acquired data $y_{i}$ as a forward model) and also that none of the columns are empty (every voxel is observable).

In comparison to ART, SART add information about an inherant noise, and its correlation matrix $\vec{\Sigma}=\vec{R}^{-1}$, as presented in \autoref{sec:ML_corr}.
This make the SART method less sensible to noise, but can also reduce the convergence speed.
In the same way, $\vec{C}$ can be understand as a pre-conditioner applied on the system.
In practise, this conditioner could also be set to $\vec{C}=\rho\vec{I}$. with $\rho=\frac{1}{\|M\|_{1}}$, this is known as the PSIRT method, and is usefull in the Ordered Subset scheme \cite{gregor_computational_2008}.

With more equations than dimensions to reconstruct ($M>N$), and the projections $y_{1},\cdots,y_{M}$ are corrupted by noise, no unique solution exists in this case. The estimation of the solution will oscillates in the neighborhood of the intersections of the hyperplans.


With less equations than dimensions to reconstruct ($M<N$), there is no unique solution, and the reconstruction result depends on the initialization.
Tanabee \cite{tanabe_projection_1971} has shown that in this case, the solution that the solution converge towards $\x^{(s)}$ such that $\|x^{(0)}-x^{(s)}\|$ is minimized.

\subsection{SART and ART: comparison and improvement}

\paragraph{Example}
If the problem with only 2 dimension, like for \eqref{eq:sys2d}, it easily represent the reconstruction graphically on \autoref{fig:ART-2D}.

\begin{equation}
  \label{eq:sys2d}
  \begin{matrix}
     a_{11}x_{1}+ a_{12}x_{2}= y_{1} \\
     a_{21}x_{1}+ a_{22}x_{2}= y_{2} \\
  \end{matrix}
\end{equation}
\begin{figure}[hbt]
  \centering
  \begin{subfigure}{0.5\linewidth}
    \centering
    \input{tikz/art.tikz}
  \subcaption{\label{fig:ART-2D}ART}
  \end{subfigure}%
  \begin{subfigure}{0.5\linewidth}
    \input{tikz/sart.tikz}
    \subcaption{\label{fig:SART-2D}SART}
  \end{subfigure}
  \caption[Algebraic solver principle]{Algebraic Solver reconstruction with N=2.the SART method  clearly converges in less iteration than ART. Furthermore the first projection of the ART (\subref{fig:ART-2D}) could have been on the other hyperplan, where the estimate would have been closer to the solution (roughly to $\x^{(5)}$ position), and the convergence would have been faster.}
\end{figure}

\subsubsection{Orthogonality and Convergence speed}

 In the ART method, If the hyperplans are perpendicular to each other, then the methods converges in $M$ steps, for any initial guess point. That will be one iteration of SART.
On the other hand, if hyperplan have only a small angle between them, the number of iterations will be large to attain the solution.
One could think of firstly orthogonalizing the system (with the Graham Schmitt procedure for example), but this become too expensive with higher dimensions. Besides, orthogonalization tends to enhance the effect of noise.

However, a few optimisation can be made: Ramakrishnan \emph{et al.} \cite{ramakrishnam_orthogonalization_1979} proposed a pair-wise orthogonalization of the equations.
A simpler approach is to use equations (ie rays) that have a wider angle between them. Introducing completely new/different information at each step.
Indeed, two geometrically close rows are prone to share common information as they probe the same area. Choosing widely separated rays, will then improve the convergence rate of the reconstruction.

Such optimization can also be made for the SART method, and have leads to used Ordered Subset\footnote{also known as Block Iterative (BI)} (OS) for speed and quality improvement.

\subsubsection{Relaxation}

Both ART and SART method can be improved by the introduction of a relaxation parameter:

\begin{equation}
  \label{eq:SART_relax}
  \x^{(k+1)} = \x^{(k)}+ \lambda_{k}\vec{C}\M^{T}\vec{R}(\y-\M\x^{(k)})
\end{equation}

The relaxation parameter $\lambda_{k}$ can be change between iteration, and the search of an optimal one (improving both reconstruction time and quality of the result) is not an easy task. Ensuring convergence leads to the constraint: $0<\\lambda _{k}<2$.
Among the different strategies used to find such relaxation parameters, so called \emph{line search} and \emph{Diminish step-size} presented in the AIR Toolbox \cite{hansen_air_2018}.

% \subsubsection{Dampening}
% \todo[inline]{Dampening of SART}

\section{Regularization}

As presented at the beginning of this chapter, the inverse problem $\y \M\x$ is is \emph{ill-posed} and also \emph{ill-conditioned}. If using the surrogate problem gives acces to a well-posed problem, it is still often ill-conditioned. The goal of regularization is thus to tackle this situation by adding extra (\ie prior) information to the problem, and by so computing a \emph{Maximum a posteriori} solution.

\subsection{Thikhonov Regularisation}

The simplest regularization  method is known as the \emph{Thikonov} regularization.

\begin{equation}
  \label{eq:thikhonov}
  \argmin{x}{f(\x,\y) + \|\vec{D}\x\|_{\vec{\Gamma}}^{2}}
\end{equation}
where $\|\vec{D}\x\|_{\vec{\Gamma}}^{2}$ is the prior knowledge term, here meaning that within a certain base\footnote{or transform, in a analytic framework} $\vec{D}$ the signal $\vec{D}\x$ is normally distributed with a covariance matrix $\vec{\Gamma}$.

often the regularisation matrix is assumed diagonal ($\vec{\Gamma} = \lambda \vec{I}$, and $\lambda>0$), and the problem becomes :

\begin{equation}
  \label{eq:thikhonov2}
  \argmin{x}{f(\x,\y) + \lambda \|\vec{D}\x\|^{2}}
\end{equation}
In practice the Thikonov Regularization guarantees somme kind of smoothness on the solution, with \eqref{eq:thikhonov2} large component of $\vec{D}\x$, also called \emph{outlier} will be penalized, and thus not appear in the final solution.

\subsection{Choice of regularization base}

The choice of the matrix $\vec{D}$ is a major step in the modelling of the problem, as it encode the prior information available on the solution.

The simplest regularisation is by setting: $\vec{D}=\vec{I}$. This will guarantee a smallest norm $\x$, which could be interpreted as the less energetic solution, and thus the most natural one. This is of great advantages with an underdetermined system, where multiple solution exist and only one should be choosen.

An other popular choice is $\vec{D}=\vec{\nabla}$ the spatial gradient. In this case, few abrupt variations (or discontinuity) can be enhance, and small but numerous noisy variation can be smeared out. This results in more piece-wise image, and in the case of tomography help to magnify the heterogeneity of the medium.

One could also use Discrete Fourier Transform (DFT) or Discrete Wavelet Transform (DWT) and encode prior information in this domain $\vec{D}=\vec{W}$.
In particular, Wavelet transform can leads to similar result as a gradient based regularisation, but also give more possibilities of tuning.
However the need of simple and fast to implement solution for this topic makes the exhaustive studies of these possibilities out of the scope of this report. A deeper analysis of the impact of regularization onto Iterative reconstruction can be found in \cite{paleo_iterative_2017}.

\subsubsection{Norm Choice}

In addition to the choice of the base of expression of the prior $\vec{D}$, the choice of the regularization norm is also of great interest.
For example the used of the classic $\ell_{2}$ is often associated with a normally distributed associated noise, and have the good taste to be mathematically easy to manipulate, and have homogeneous behaviour.
More Recently, the used of the $\ell_{1}$ norm became widespread in the literature. In the bayesian framework this is associated with a Laplace distribution in the $\vec{D}$ basis. The $\ell_{1}$ regularisation is less sensitive to outlier values (being not squared up), however its non differentiability makes it impossible to gives explicit formula of the solution.


\subsection{Total Variation}

In combination with $\vec{D}=\vec{\nabla}$ the use of the $\ell_{1}$ is known as the \emph{Total Variation} (TV) regularization producing the following problem:
\begin{equation}
  \label{eq:TV_prob}
  \argmin{x}{\|\y-\M\x\|_{2}^{2} + \mu \|\vec{\nabla}\x\|_{1}}
\end{equation}
Total Variation has been successfully used in the image reconstruction and inverse problem fields.
This has been theorized in the \emph{Compress Sensing} framework \cite{foucart_mathematical_2013}.
In practice, Total Variation (and more generally $\ell_{1}$ regularisation) encourage some kind of sparsity in the solution in basis $\vec{D}$.
In the discrete 2D and 3D cases, the computation of the total variation can be done  in two ways:
\begin{itemize}
  \item The \emph{anisotropic} total variation:
    \begin{equation*}
      TV_{aniso}(\x) = \sum_{i,j}^{}|g_{i,j}^{(1)}| + |g_{i,j}^{(2)}| +|g_{i,j}^{(3)}|
    \end{equation*}

  \item The \emph{isotropic} total variation:
    \begin{equation*}
      TV_{iso}(\x) = \sum_{i,j}\sqrt{(g_{i,j}^{(1)})^{2}+(g_{i,j}^{(2)})^{2}+(g_{i,j}^{(3)})^{2}}
    \end{equation*}
\end{itemize}

The use of an isotropic or anisotropic TV have an impact on the reconstructed image, enforcing (or not) the isotropism of the medium, in particular, isotropic TV promotes more circle/sphere like shape (\ie corner less) whereas the anisotropic TV is prone to yields  square like shape, oriented in the gradients axis. The higher the regularisation parameter $\mu$ is, the more this effect can be seen. Moreover, the sparsity of the gradient produces a piece-wise reconstruction image, and this property will be even more visible with stronger regularization.

The hypothesis of a piece wised image make sense in the USCT case, as the aperture medium can be separated in different parts: water background, fatty tissues, glandular tissues, tumours, etc...  However, assuming that it an absolute truth should be avoided: In the tissues above, there should be some variations (but in a smaller range than between two categories of tissues). A too strong regularization will unfortunately neglect these variations.


\section{Total Variation Augmented Lagrangian (TVAL)}
\label{sec:TVAL}

\subsection{An Optimisation Problem}
Considering that the gradient of $\vec{x}$ is sparse, the problem to solve can be expressed as:

\begin{equation}
  \label{eq:B_invp}
  \min \|\vec{D_{x}x}\|_{1}+\|\vec{D_{y}x}\|_{1}+\|\vec{D_{z}x}\|_{1} \text{ with } \vec{y}=\vec{Mx}
\end{equation}
Where $\vec{D_{x}},\vec{D_{y}},\vec{D_{z}}$ are the discrete gradient  operator in the corresponding direction.
Hereafter $\vec{w}_{i} = \vec{D}_{i}\vec{x} $.

\eqref{eq:B_invp} is an optimisation problem, that can be solve by introducing the Lagrangian operator:
\begin{equation}
  \label{eq:B_lag}
\begin{aligned}
  \mathscr{L}(\beta,\mu,\vec{\lambda},\vec{\nu})=&
  \underbrace{\sum_{i}\left(
      \left\|\vec{w}_{i}\right\|
      - \nu_{i}^{T}(\vec{D_{i}} \vec{x}-\vec{w}_{i})
      +\frac{\beta}{2}\|D_{i}\vec{x}-\vec{w}_{i}\|_{2}^{2}
    \right)}_{\text{w-problem}}\\
 & - \underbrace{\vec{\lambda}^{T}(\vec{M} \vec{x}-\vec{b})+\frac{\mu}{2}\|\vec{M} \vec{x}-\vec{b}\|_{2}^{2}}_{\text{x-problem}}
\end{aligned}
\end{equation}
The introduction of $\vec{w}$ is mandatory as the TV minimisation can not be differentiated. and one needs to solve the $w$-problem, which  is also a optimisation problem. It also guarantees a bounded solution\cite{li_efficient_2010}.

\subsection{The algorithm}

In an interative way, compute $\vec{w}_{k+1}$, $\vec{x}_{k+1}$ and update the Lagrangian multiplicators.
\begin{equation}
  \label{eq:B_w-pb}
  \left\|\vec{w}_{i,k+1}\right\|-\nu_{i}^{T}\left(D_{i} \vec{x}_{k}-\vec{w}_{i,k+1}\right)+\frac{\beta}{2}\left\|D_{i} \vec{x}_{k}-\vec{w}_{i,k+1}\right\|_{2}^{2}
\end{equation}
\eqref{eq:B_w-pb} is the case of the $l_{1}$ norm \eqref{eq:B_w-pb} can be explicitly solve:
\begin{equation}
  \label{eq:B_w-sol}
  \vec{w}_{i, k+1}=\max \left\{\left|D_{i} \vec{x}_{k}-\frac{\nu_{i}}{\beta_{i}}\right|-\frac{1}{\beta_{i}}, 0\right\} \operatorname{sgn}\left(D_{i} \vec{x}_{k}-\frac{\nu_{i}}{\beta_{i}}\right)
\end{equation}
the proof and the extended solution in $l_{2}$-norm case can be found in \cite{li_efficient_2010}. Knowing the value $\vec{w}_{k}$ given by \eqref{eq:B_lag}  $\vec{x}_{k}$ can be determined, coming from zeroing its gradient.

\begin{equation}
  \label{eq:B_x-pb-sol}
  \begin{aligned}
    \vec{x}_{k+1} &=\left(\sum_{i} \beta_{i}\vec{ D_{i}}^{T}\vec{ D_{i}}+\mu \vec{M}^{T} \vec{M}\right)^{\dagger}\\
    & \times \left(\sum_{i}\left(\vec{D_{i}}^{T} v_{i}+\beta \vec{D_{i}}^{T} \vec{w}_{i, k+1}\right)+\vec{M}^{T} \lambda+\mu \vec{M}^{T} \vec{b}\right)
  \end{aligned}
\end{equation}
Where $()^{\dagger}$ is the Moore-Penrose Pseudoinverse.
Unfortunately, computing such inverse at each iteration is too costly to be numerically implemented, to solve such problem the one-step steepest descent method is choosen. The first iteration of the steepest descent may not be very close to the real solution, but the alternatively solving of $\vec{w}$ and $\vec{x}$ gives a converging solution.


\subsubsection{Implementation}
A lot of effort has already been put in the implementation of the TVAL3 algorithm, going from a \texttt{CPU-MATLAB-double }version (around 80s of compute time) to a \texttt{GPU-C++-float} version (<10s of compute time). More information about this optimisation can be found in \cite{birk_gpu-based_2014}.

\subsection{A Bayesian Explaination to TVAL}

The following try to give a meaning to the regularisation parameters introduced in the TVAL3 algorithm.

Let consider the inverse problem disturbed by $\vec{n}$ a Gaussian noise of law $ \mathcal{N}(0,\sigma_{n})$:
\begin{equation}
  \label{eq:bayes1}
  \vec{y} = \vec{Mx + n}
\end{equation}
Then with a probabilistic approach to the problem:
\begin{equation}
  \label{eq:bayes2}
  p(\vec{y}|\vec{x}) = \frac{1}{(2\pi\sigma)^{N/2}} \exp \left(- \frac{\|\vec{y-Mx}\|}{2\sigma_{n}} \right)
\end{equation}

With the assumption of the sparsity of the gradient $\vec{w} = \vec{D}\vec{x}$. Where $\vec{D}$ is the differential operator in 3D, $\vec{D}$ is a $3N \times N$ linear operator, but not invertible.

In terms of probability the sparsity of $\vec{w}$ can be express by using an exponential law of parameter $\mu_{\vec{w}}$.

\begin{equation}
  \label{eq:bayes3}
  p(\vec{w}) = \exp(-\mu_{\vec{w}}\|\vec{w}\|_{1})
\end{equation}

Knowing $\vec{w}$,  $\vec{x}$ can be determined by using the adjoint operator of $\vec{D}$.
\begin{equation}
  \label{eq:bayes4}
  \vec{x} = \vec{D}^{\dag} \vec{w} + \vec{\bar{x}}
\end{equation}

With $\vec{\bar{x}}$ the unknown mean value of $x$. Without loss of generality let $\vec{\bar{x}}$ follow a Gaussian law $\mathcal{N}(\vec{\mu_{x}},\sigma_{x})$:
\begin{equation}
  \label{eq:bayes5}
    \begin{aligned}
  p(\vec{x}|\vec{w}) & = \frac{1}{(2\pi\sigma)^{3N/2}} \exp \left(-
                       \frac{\|\vec{x-D^{\dag}\vec{w}}-\vec{\mu_{x}}\|^{2}_{2}}{2\sigma_{x}} \right) \\
                     & = \frac{1}{(2\pi\sigma)^{3N/2}} \exp \left(-
                       \frac{\|\vec{Dx-\vec{w}} -\vec{D\mu_{x}}\|^{2}_{2}}{2\sigma_{x}} \right)   \\
                     & = \frac{1}{(2\pi\sigma)^{3N/2}} \exp \left(-
                       \frac{\|\vec{Dx-\vec{w}}\|^{2}_{2}}{2\sigma_{x}} \right)
\end{aligned}
\end{equation}


Using \eqref{eq:bayes2},\eqref{eq:bayes4} and \eqref{eq:bayes5}:

\begin{equation}
  \label{eq:bayes6}
  \begin{aligned}
    p(\vec{y}) &= p(\vec{y}|\vec{x})p(\vec{x}|\vec{w})p(\vec{w}) \\
    &\propto \exp \left( -\underbrace{\left( \frac{\|\vec{y-Mx}\|}{2\sigma_{n}} + \frac{\|\vec{Dx-\vec{w}}\|^{2}_{2}}{2\sigma_{x}} +\mu_{\vec{w}}\|\vec{w}\|_{1}\right)}_{J(\vec{x})} \right)
  \end{aligned}
\end{equation}
\section{Summary}


The inverse problem modelized in the previous chapter cannot be solve by a direct inversion, and needs an interative method. The SART method has been choose for its robustness and its wide use in the literature. To counteract the noise sensibility of this method, some previous knowledge is introduced by using a Total Variation regularisation.  In practice the two method are run successively. the USCT project has also been relying on the TVAL3 algorithm, where the TV regularisation is done during the reconstruction.

SART and  an isolated TV regularisation are both well known methods in the literature, and there already exists efficient implementation of them, and strategies to fine tune their parameters

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
