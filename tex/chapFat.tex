\documentclass[main.tex]{subfiles}
% in thi chapter present the data flow of usct, where we work on this flow,
% sum up the rest of the signal/image processing for the curiosity
\begin{document}
\chapter{Finite frequency tomography}
\label{chap:fat}

In \autoref{chap:phys} we saw that the Fresnel Zone defines a volume in which our rays gather information.
The Fresnel zone is frequency dependent, and where our previous rays approximation assumed an infinite frequency, the \emph{fat ray} will assume a \emph{finite and fixed frequency}.

Originally used in geophysics and seismic tomography~\cite{cerveny_fresnel_1992,husen_local_2001,spetzler_fresnel_2004}, I adapted it to the case of ultrasound transmission tomography .
The main idea behind fat ray tomography is to spread the ray among more pixels than with a classic, single-pixel wide ray, by considering also the pixel along the ``side'' of the ray in the Fresnel Zone, having more entries in the ray matrix, and reducing it sparsity.

Furthermore, the speed of sound distribution can vary inside the Fresnel zone, the use of the fat ray allows us to go beyond this limitation.

\section{Defining the Fresnel Zone}
\label{subsec:determineFZ}
The $n-$ Fresnel Zone around a  straight ray in a homogeneous medium, can be defined as the ellipse which focal point are the emitter and receiver.
Odd-numbered Fresnel Zone allows for constructive interference ($<\lambda/2$), even-numbered zone makes destructive interference. In the following will refer only of the first Fresnel Zone, or just Fresnel Zone.

For more complex medium one should return to the original definition of the Fresnel zone: the region between the source and receiver for which the scattered wave field contributes constructively at the receiver position.

\begin{equation}
  \label{eq:fresnel_time}
  \delta \tau = \tau(EF)+\tau(FR) - \tau(ER) \le \frac{1}{2 f}
\end{equation}

Where $\tau(EF), \tau(FR), \tau(ER)$, are the time of flight on the according paths.

\begin{figure}[hbt]
  \centering
  \begin{tikzpicture}
    \setlength{\mywidth}{0.4\textwidth}
    \node (TS) {\input{plot/FMM_1.tex}};
    \node[right=0.2\textwidth,anchor = west] (TR) at (TS) {\input{plot/FMM_2.tex}};
    \node[right=0.2\textwidth,anchor = west] (TM) at (TR) {\input{plot/FMM_3.tex}};
    \path (TS) -- (TR) node[midway,below]{\textbf{+}};
    \path (TR) -- (TM) node[midway,below]{\textbf{=}};
  \end{tikzpicture}
  \caption[Time defined fat ray]{Construction of the fat ray using the time of flight between an emitter and a receiver.
    The dashed line represents the fastest path, passing by the minimum of the map.}
  \label{fig:fatray}
\end{figure}


\subsection{Paraxial approximation}
The time defined Fresnel volume also has a  geometrical definition, that can be used to determining the size of it.
Without loss of generality one can consider the straight ray case.

The equation \eqref{eq:fresnel_time} is rewritten as:
\begin{equation}
  \label{eq:fresnel_dist}
\delta l  =   l(EF)+l(FR) - l(ER) \le \frac{\lambda}{2}
\end{equation}

On the boundary of the  Fresnel zone  its radius can be computed by making the \emph{paraxial approximation} $r \ll x,L-x$ :
\begin{equation*}
  \sqrt{x^{2}+r(x)^{2}}+\sqrt{(L-x)^{2}+r(x)^{2}} - L \simeq \frac{1}{2}r(x)^{2} \frac{L}{(L-x)x}   =   \frac{\lambda}{2}
\end{equation*}
Finally the radius of the Fresnel zone can be approximate by:
\begin{equation}
  \label{eq:fresnel_radius}
  r(x) = \sqrt{\frac{\lambda(L-x)x}{2L}}
\end{equation}
It is obvious from \autoref{fig:fresnel_parax} that the paraxial Fresnel volumes are good, sufficiently accurate approximations for the exact Fresnel volumes.
Only close to emitter and receiver, particularly for low frequencies, the accuracy is lower.
The differences in the vicinity E and R are discussed in greater detail.

\subsection{Limits of the paraxial approximation}

\begin{figure}[hbt]
  \centering
  \input{tikz/fresnel_parax.tikz}
  \caption[Paraxial approximation in the Fresnel zone]{\label{fig:fresnel_parax} 2D projection of the real Fresnel Zone (dotted line) and its paraxial approximation  delimitation. The approximation holds only in the middle of the ellipse.}
\end{figure}

At the focal point (E and R) the exact radius of the fresnel zone is:
\begin{equation}
  \label{eq:fresnel_trueradius}
  r_{exact}(E) = r_{exact}(R) =  \frac{\lambda}{2} \frac{1+\lambda/4L}{1+\lambda/2L}
\end{equation}
the approximation $\lambda \ll L$ leads to $r \simeq \frac{\lambda}{2}$.
In the paraxial computation, however, the radius shrinks to zero, as well as for the ``overshooting'' after the receiver/emitter (it is $\lambda/4$  without the paraxial approximation).

The paraxial approximation holds still in the middle of the ray.
It could therefore be used to characterize the Fresnel Zone, and is in some cases \cite{jocker_validation_2006,snieder_wavefield_1996,spetzler_fresnel_2004} also use to compute sensibility Kernel (see \autoref{sec:kernel}).

\section{Linear Interpolation}
For a fat ray, the time of an arrival is influenced by all the voxels inside the Fresnel Zone, therefore, equation \eqref{eq:Lin2} becomes a volume integral:

\begin{equation}
  \label{eq:fatray1}
  t_{k} =  \int_{V} K_{t}(\x) \frac{1}{c(\x)} \d \x
\end{equation}

Where $K_{t}(\x)$, is the time sensibility Kernel assimilated to the inverse of an area, stating how each voxel is contributing to the fat ray.
Without further information, $K(\x)$  could be considered as a cross section area of the fat ray perpendicular to the central ray. Every point in the ray has hence an equal contribution to the travel time.
In a discretized and linearized form it can also be understood as the sensitivity of $t_{k}$ with respect to change in the slowness distribution, building a Jacobian Matrix.
In this case the partial derivative with respect to a change in velocity $c_{i}$:

\begin{equation}
  \label{eq:fatray2}
  \derivp[t_{k}]{c_{i}} = -\frac{v_{i}}{V}  \frac{t_{k}}{c_{i}}
\end{equation}
Where $v_{k}$ is the volume of the cell, and $V$ the total volume of the fat ray. By using the chain rule the sensitivity term can be expressed in term of slowness, and this form will be used in the reconstruction software.

\begin{equation}
  \label{eq:fatray3}
    \derivp[t_{k}]{s_{i}} = \derivp[t_{k}]{c_{i}}\derivp[c_{i}]{s_{i}}
    =  \derivp[t_{k}]{v_{i}} \left(- \frac{1}{s_{i}^{2}} \right)
    =  \frac{v_{i}}{V} \frac{t_{k}}{s_{i}}
\end{equation}


A first improvement of this flat kernek it to  modulate it  by a linear attenuation \cite{watanabe_seismic_1999}  from the center of the fat ray, even if such adjustment lack of a physical ground.
\begin{equation}
  \label{eq:fatray4}
  w(\tau) =
  \begin{cases}
    1- 2 f \Delta \tau & (0\leq\Delta t \leq 1/2f) \\
    0 & (1/2f \leq\Delta t)
  \end{cases}
\end{equation}

giving the weight in the matrix for the $i-$th ray and the $j$-th pixel
\begin{equation}
  \label{eq:fatray5}
  m_{k,i} = \frac{w(\tau_{i})}{\sum_{}^{}w(\tau_{i})
}\frac{v_{i}}{V}\frac{t_{k}}{s_{i}}
\end{equation}


\section{Wave-based approximation}
\label{sec:kernel}
The Fat Ray theory make the assumption of a finite frequency, and numerous research have been done with this hypothesis, especially in geophysics.
The following is a development about the sensitivity kernel \cite{snieder_wavefield_1996,spetzler_effect_2001,jocker_validation_2006}.

According to the Rytov Approximation (see \autoref{subsec:rytov}) the pressure field received at position $\x_{r}$ (emitted from $\x_{e}$ at pulsation $\omega$)

\begin{gather}
  P_{r}(\x_{r},\x_{e},\omega) = P_{i}(\x_{r},\x_{e},\omega) \exp \left(\frac{P_{B}(\x_{r},\x_{e},\omega)}{P_{i}(\x_{r},\x_{e},\omega)} \right)
  \tag{\ref{eq:Acc16} again}
\end{gather}

From \eqref{eq:Acc16} the phase shift between the emitted and received pulse can be determined as:

\begin{equation}
  \label{eq:rytov1}
  \delta \varphi(\x_{r},\x_{e})  = \mathrm{Im}\left[\frac{P_{B}}{P_{i}} \right]
\end{equation}

For a fixed frequency  \eqref{eq:rytov1} can be expressed  as a time difference:
\begin{equation}
  \label{eq:rytov2}
  \delta t = \frac{1}{\omega} \delta \varphi = \frac{1}{\omega} \mathrm{Im}\left[\frac{P_{B}}{P_{i}} \right]
\end{equation}

At the receiver, the Born pressure field (scattered wave) and the initial pressure field are express as:

\begin{gather}
  \tag{\ref{eq:Acc15} again}
  P_{B}(\x_{r},\x_{e}) =  \int_{\Omega}G(\x_{r},\y) k_{0}^{2}\delta n(\y)P_{i}(\y,\x_{e},\omega)\d\y
\end{gather}
\begin{equation}
  \label{eq:rytov3}
  P_{i}(y,\x_{e})  = P_{0} G(\y,\x_{e})
\end{equation}

By using \eqref{eq:rytov3} and \eqref{eq:Acc15}  the phase term of \eqref{eq:Acc16} is computed as:

\begin{equation*}
  \frac{P_{B}}{P_{i}}(\x_{r},\x_{e}) = \int_{\Omega}\frac{-2 \omega^{2}\delta c(\y)}{c_{0}^{3}}
  \frac{G(\vec{x_{r}},\y)G(\y,\x_{e})}{G(\x_{r},\x_{e})}
  \d \y
\end{equation*}
And with~\eqref{eq:rytov1} and~\eqref{eq:rytov2}:

\begin{equation}
  \label{eq:rytov4}
  \delta t (\x_{r},\x_{e}) = \int_{\Omega}^{}\underbrace{%
    \frac{-2 \omega}{c_{0}^{3}}\mathrm{Im}\left[
      \frac{G(\x_{r},\y)G(\y,\x_{e})}{G(\x_{r},\x_{e})}
    \right]
  }_{K_{t}(\y,\omega)}\delta c(\y) \d \y
\end{equation}

From \autoref{chap:phys}:
\begin{align*}
  c &= c_{0} + \delta c \\
  s &= \frac{1}{c} =s_{0} + \delta s \\
  \delta c &= c - c_{0} = \frac{1}{s_{0}+\delta s}-\frac{1}{s_{0}} \simeq \frac{-\delta s}{s_{0}^{2}}
\end{align*}
And \eqref{eq:rytov4}  is rewritten to work with slowness:
\begin{equation}
  \label{eq:rytov5}
  \delta t (\x_{r},\x_{e}) = \int_{\Omega}^{}\underbrace{%
    2 \omega s_{0}\mathrm{Im}\left[
      \frac{G(\x_{r},\y)G(\y,\x_{e})}{G(\x_{r},\x_{e})}
    \right]
  }_{K_{t}(\y,\omega)} \delta s(\y) \d \y
\end{equation}

\subsection{3D Kernel}

In 3D the Green function is defined as:
\[
  G(\x,\y) = \frac{1}{4\pi}\frac{e^{ik_{0}\|\x-\y\|}}{\|\x-\y\|}
\]
by reusing the notation of \eqref{eq:fresnel_dist}:

\begin{equation}
  \label{eq:Kernel0}
  K_{t}^{3D}(\y,\omega) = \frac{\|\x_{r}-\x_{e}\|}{\|\vec{x_{r}}-\y\|\|\y-\x_{e}\|} s_{0} \frac{\omega}{2\pi} \sin(\omega s_{0}\delta l(\y))
\end{equation}

This formulation of the sensibility Kernel $K(\y,\omega)$ only assumes that there is a homogeneous speed of sound for the wave propagation and a mono frequency  pressure fields and only one scattering point for each ray in the Fresnel zone.

The mono frequency hypothesis can be dropped by considering the spectrum of the pressure field:

\begin{equation}
  \label{eq:Kernel1}
  \boxed{%
    K^{3D}_{t}(\y) =  \frac{s_{0}}{2\pi}
    \frac{\|\x_{r}-\x_{e}\|}{\|\x_{r}-\y\|\|\y-\x_{e}\|}
    \int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\omega\sin(\omega s_{0} \delta l(\y)) \d \omega
  }
\end{equation}
With $\int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega) = 1 $ and $A(\omega)$ null outside $\omega \pm \Delta\omega$.

\subsection{2D Kernel}
When restraining us to a 2D reconstruction, the Green function is impacted (use of a cylindrical wave  instead of a spherical wave). Using the far field approximation ($\|y-x_{e}\| \gg \lambda$)

\[
  G_{2D}(\x,\y)  = \frac{-1}{\sqrt{8\pi\|\x-\y\|}}\exp{\left(ik_{0}(\|\x-\y\|)+i \frac{\pi}{4}\right)}
\]


With this expression of the Green function, the sensibility Kernel become:


\begin{equation}
  \label{eq:Kernel3}
  \boxed{%
  K^{2D}_{t}(\y) =  \sqrt{\frac{s_{0}}{2\pi}}
  \sqrt{\frac{\|\x_{r}-\x_{e}\|}{\|\x_{r}-\y\|\|\y-\x_e\|}}
  \int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\sqrt{\omega}\sin\left(\omega s_{0} \delta l(\y) + \frac{\pi}{4}\right) \d \omega
  }
\end{equation}

\subsection{Attenuation Kernel}

As with the simple ray theory an attenuation sensibility Kernel can also be computed, using the amplitude variation of the Rytov pressure field $A_{r}$ with respect to the reference amplitude $A_{0}$.

\begin{equation}
  \label{eq:AttKern1}
  \ln \left( \frac{A_{r}}{A_{0}}(\x_e,\x_{r}) \right) = \mathrm{Re} \left[\frac{P_{B}}{P_{i}} \right]
\end{equation}
for small variation  $A_{r} = A_{0} + \delta A $ and so :
\begin{equation}
  \label{eq:AtttKern2}
  \frac{\delta A}{A_{0}} \simeq \mathrm{Re} \left[\frac{P_{B}}{P_{i}} \right] = \int_{\Omega}^{}\delta s K_{A}(\y) \d \y
\end{equation}
\begin{itemize}
  \item In 3D:
\begin{equation}
  \label{eq:AttKern3}
  \boxed{%
    K^{3D}_{A}(\y) =  \frac{s_{0}}{2\pi}
    \frac{\|\x_{r}-\x_e\|}{\|\vec{x}_{r}-\y\|\|\y-\vec{x}_{e}\|}
    \int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\omega^{2}\cos(\omega s_{0} \delta l(\y)) \d \omega
  }
\end{equation}
  \item In 2D:
\begin{equation}
  \label{eq:AttKern4}
  \boxed{%
    K^{2D}_{A}(\y) =  \sqrt{\frac{s_{0}}{2\pi}}
    \sqrt{\frac{\|\x_{r}-\x_e\|}{\|\vec{x_{r}}-\y\|\|\y-\x_e\|}}
    \int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\omega^{3/2}\cos(\omega s_{0} \delta l(\y)) \d \omega
  }
\end{equation}
\end{itemize}

As shown on \autoref{fig:kernel}, considering a broad band signal highly decreases the variations inside the sensitivity kernel (due to beat interference).
Furthermore, in 3D the kernel is not influenced by the pixels along the direct path (straight ray), as if the ray did not take its own path.
This is phenomenon is known as the banana-doghnut paradox in the literature of seismics~\cite{tian_dynamic_2007,jocker_validation_2006,spetzler_fresnel_2004,tromp_seismic_2005}.

\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{0.5\textwidth}
  \setlength{\myheight}{0.6\mywidth}
  \subcaptionbox{
    Time Kernel 2D
  }[0.5\textwidth]{
    \input{plot/kernel/kernel_1}
  }%
  \subcaptionbox{
    Time Kernel 3D
  }[0.5\textwidth]{
    \input{plot/kernel/kernel_2}
  }
  \subcaptionbox{
    Attenuation Kernel 2D
  }[0.5\textwidth]{
    \input{plot/kernel/kernel_3}
  }%
  \subcaptionbox{
    Attenuation Kernel 3D
  }[0.5\textwidth]{
    \input{plot/kernel/kernel_4}
  }
  \caption[Cross Section of Sensibility Kernel]{Cross section of the presented sensibility kernels (normalized amplitude), the thick blue line represents the averaged Kernel, black line are kernel computed for different frequencies from \SI{0.5}{MHz} to \SI{2.5}{MHz}. The higher is the sampling frequency (in space and time), the smoother is the Kernel.}
  \label{fig:kernel}
\end{figure}

\section{Discrete Scheme}

All the integrals defined previously for the fat ray needs to be discrete, for allowing a numerical approximation.

\subsection{Numerical Optimisation}
Furthermore, the sensibilities kernels are computed for the complete reconstruction space, this full wave approach would be too expensive to compute, and reducing them  only to the first Fresnel Zone help to speed the computation process, as well as reducing the influence of noisy data.

\subsection{Wave-based Kernel}

From  the continuous Kernel expression:

\begin{gather}
  \tag{\ref{eq:rytov4} again}
  \delta t(\x_r,\x_e) = \int_{\Omega} \delta s (\y) K(\y,\omega) \d \y
\end{gather}

With $\Delta x, \Delta y, \Delta z$ the resolution step of the different  reconstruction space dimensions the elementary volume is $\d y \simeq \Delta x \Delta y \Delta z$ and in the same way  $\d \omega = \Delta \omega$

For the $k$-th ray, in its Fresnel Zone $\mathcal{F}_{k}$, in 3D:

\begin{equation}
  \label{eq:discret1}
  \delta t_{k} = \Delta x \Delta y \Delta z \Delta\omega \frac{s_{0}}{2\pi}
  \sum_{y_{i}\in \mathcal{F}_{k}}
  \sum_{j}\frac{\vec{d}_{k}}{\vec{E}_{k,i}\vec{R}_{k,i}} A(\omega_{j}) \omega_{j} \sin(\omega_{j} s_{0}\vec{E}_{k,i}+\vec{R}_{k,i}-\vec{d}_{k} )
\end{equation}

Where $\vec{E}$ and $\vec{R}$ are  the matrix of distance between every pixel and emitter or receiver; $\vec{d}_{k}$ the straight distance between an emitter and a receiver.

These matrices can be pre-computed, as for a fixed emitter (resp.\ a fixed receiver) the distance between it and every pixel can be reused for every other ray with the same emitter (resp.\ receiver).

The same expression can similarly be found for the other Kernels.

\subsection{Resolution Step}

Another aspect of discretely computing the kernels, is the resolution steps (\ie pixel size).
On \autoref{fig:kernel} the represented kernels are very smooth, and the high frequency variation are quite visible.
By decreasing the resolution, one can see on \autoref{fig:kernel_comp_size} that the sensitivity kernel could be approximated by a simpler, faster to compute function (\eg{} a trapezoidal shape for the 2D kernel in \autoref{fig:kernel_comp_size_64} and \ref{fig:kernel_comp_size_128}).

\begin{figure}[hbt]
  \centering
  \setlength{\mywidth}{0.5\textwidth}
  \setlength{\myheight}{0.707\mywidth}
  \subcaptionbox{\label{fig:kernel_comp_size_64}
    64px, 1px = \SI{4}{mm}}[.5\textwidth]{\input{plot/kernel/kernel_res_64}}%
  \subcaptionbox{\label{fig:kernel_comp_size_128}
    128px, 1px = \SI{2}{mm}}[.5\textwidth]{\input{plot/kernel/kernel_res_128}}
  \subcaptionbox{256px, 1px = \SI{1}{mm}}[.5\textwidth]{\input{plot/kernel/kernel_res_256}}%
  \subcaptionbox{512px, 1px = \SI{0.5}{mm}}[.5\textwidth]{\input{plot/kernel/kernel_res_512}}
  \caption[Influence of the resolution step on Kernel]{\label{fig:kernel_comp_size}The resolution step greatly influence the overall aspect of the kernel}
\end{figure}

Using a 128 or a 256 pixel wide grid is again a good deal between speed and resolution power, the lobes of the kernel are visible, and the computation on a standard 4-core CPU is below a minute of computation.
A MATLAB implementation and a C implementation of the 2D Kernel computation has been realized.
The latter being memory efficient it could be extended to a 3D computation.
However, it is still a monothreaded computation, and is therefore slower than the MATLAB vectorized version.
A Huge improvement would be to used dedicated GPU computation for this task.
In fact, the bottleneck of computation has been reduced to the sole computation of the sinus operation of \eqref{eq:discret1}, which have to be conduct $N_{\mathcal{F}}\times N_{\omega} \times N_{ray}$ in total.

Another speed and memory improvement could be conduct by using \texttt{single} (16 bits float) datatype everywhere, however as of 2020 the current version of MATLAB does not allow for sparse matrix to contain something differents than \texttt{double} (32 bits float) datatype.
\subsection{Extension of the Fat Ray framework}
\subsubsection{Fourier expression}

The expression of the Fréchet Kernel can be rewritten as:
\begin{equation}
\begin{aligned}
  K^{3D}_{t}(\y) &=  \frac{s_{0}}{2\pi{}}
  \frac{\|\x_{r}-\x_{e}\|}{\|\x_{r}-\y\|\|\y-\x_{e}\|}
  \int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\omega\sin(\omega s_{0} \delta l(\y)) \d \omega \\
  &= \frac{s_{0}}{2\pi}
  \frac{\|\x_{r}-\x_{e}\|}{\|\x_{r}-\y\|\|\y-\x_{e}\|}
  \mathrm{Re}\left\{\int_{\omega-\Delta\omega}^{\omega+\Delta\omega} A(\omega)\omega\exp(j\omega s_{0} \delta l(\y)) \d \omega \right\}\\
  &= \frac{s_{0}}{2\pi{}}
  \frac{\|\x_{r}-\x_{e}\|}{\|\x_{r}-\y\|\|\y-\x_{e}\|}
  \mathrm{Re}\left[ \mathcal{F}^{-1}\left\{A(\omega)\omega\right\}\right](s_0\delta l(\y))\\
\end{aligned}
\label{eq:KernelFourier}
\end{equation}

Where \(\mathcal{F}^{-1} \) denotes the inverse fourier transform. The expression \(\mathrm{Re}\left[ \mathcal{F}^{-1}\left\{A(\omega)\omega\right\}\right]\) is the same for every kernel. Instead of computing it for each row, a look-up table could be use to determine each pixel value for the kernel. This will likely speed the forward model construction, however, at the time of writing this report, this has not yet been implemented.

Moreover, if one has a an analytical expression for \(A(\omega)\) it nmay be possible to acces to an easier way of computing the kernel values.


\subsubsection{Higher order approximation}

Chapter \ref{chap:phys} presents how the Born pressure field could be determined by iterating a recursive scheme:

\begin{equation}
  \label{eq:born_iterate}
  P_{B}^{(k+1)}(\x) = \int G(\x,\y)(P_{B}^{k}(\y)+P_{i}(\y))f(\y)\d\y
\end{equation}

For the kernel development the first order approximation is used \ie, no previous scattering is considered. By adding a second scattering point in the kernel \eqref{eq:born_iterate} becomes:


\begin{equation}
  \label{eq:born_it2}
  \begin{aligned}
    P_{B}^{(k+1)}(\x) &= \int G(\x,\y)(P_{B}^{k}(\y)+P_{i}(\y))f(\y)\d\y \\
    &= \underbrace{\int G(\x,\y)(P_{B}^{k}(\y)f(\y)\d\y}_{\tilde{P_{B}^{(1)}}} + P_{B}^{(1)}(\x) \\
\end{aligned}
\end{equation}

By applying the same development for the first approximation:

\begin{equation}
  \label{eq:born_it3}
  \delta t =\frac{1}{\omega} \mathrm{Im}\left[\frac{\tilde{P_{B}^{(1)}}}{P_{i}}\right]+
\int K^{(1)}(\x_{e},\x_{r},\omega,\y)\delta s(\y)\d \y
\end{equation}
With:
\begin{equation}
  \frac{1}{\omega} \mathrm{Im}\left[\frac{\tilde{P_{B}^{(1)}}}{P_{i}}\right] =
  \iint \left(\frac{G(\x_{r},\y),G(\y,\y')G(\y',\x_{e})}{G(\x_{r},\x_{e})}\right) 2\omega^{3}s_{0}^{2}\delta s(\y)\delta s(\y')\d\y\d\y'
\end{equation}

Thus, the second order Kernel (for a monochromatic wave) is:

\begin{equation}
  \label{eq:kernel2ndorder}
  K^{(2)}(\y) = \frac{\|\x_{e}-\x_{r}\|}{\|\x_{r}-\y\|} \int \frac{1}{\|\y-\y'\|\|\|y'-\x_{e}\|}\sin(\omega s_{0}\delta l^{(2)}(\y'))2\omega^{3}s_{0}^{2}\delta s(\y')\d \y'
\end{equation}

So the complete sensitivity kernel is:

\begin{equation}
  \label{eq:kernel2orderfull}
  \delta t = \int K^{(2)}(\y) + K^{(1)}(y) \delta s(\y)\d \y
\end{equation}

A test implementation of this second order Kernel has been done, and bring nothing but extra computational time, more study will therefore be needed on this subject to determine the relevance of this higher order approximation.
Theoretically such approximation could be pushed further, seeking toward a complete full wave scattering, building a bridge between the Born approximation and the full wave approach.

\section{Summary}
By using the finite frequency tomography, and taking advantages of the Fresnel's zone interference the amount of information collected by a pair of emitter and receiver increases.
The rays presented in \autoref{chap:ray} are now kernels, meeting more pixels on their path.
They also put a bridge between the ray based tomography and the full wave approach.

The fat ray developed here does not need a heterogeneous initial guess (like bent rays) to provide good results (see \autoref{chap:result}).
By using a higher order approximation, such hypothesis could be added, however first tests of this methods did not show satisfactory enough results to be shown in this work.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
