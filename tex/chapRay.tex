\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Infinite frequency tomography}
  \label{chap:ray}

  The eikonal equation~\eqref{eq:Acc27} is not linear, and it is \emph{a priori} not possible to determine the refractive index (\ie{} the speed of sound) from the phase differences.
  By assuming an infinite frequency wave, we can consider an ideal ray propagation.
  Only macroscopic effects are relevant, and scattering -- happening at a microscopic scale -- is neglected.

This chapter presents the actual approach to ray-based transmission tomography, and its limits.

\section{Determining the ray path}

As we are now considering rays, rather than full waves, parallels can be drawn with the geometrical optics.
Determining the propagation time or \emph{time of flight}, is dependent of the path taking by the ray, which is assumed to be the fastest one\cite{snieder_ray_1992}.
We can express the eikonal equation \eqref{eq:Acc27} in terms of a Euler-Lagrange Equation, and solving this equation is equivalent to a variation problem:

\begin{equation}
  \label{eq:Lin1}
  \deriv[]{l} \left( \frac{1}{c} \deriv[\vec{x}]{l}\right) = \nabla \left( \frac{1}{c} \right)
\end{equation}
With $l$ the length of the ray.
Solving this equation gives the path of the ray.
With this path, we can then compute a \emph{time of flight} defined as:
\begin{equation}
  \label{eq:Lin2}
  t = \int_{\mathcal{R}} \frac{1}{c(\vec{x}(l))}\d l = \int_{\mathcal{R}} s(\vec{x})dl
\end{equation}
and introducing the slowness vector $s(\vec{x}) = 1/c(\vec{x})$.

For the reconstruction, we use the water-filled background as a reference, and so we can only compute a time shift:

\begin{equation}
  \label{eq:Lin3}
  \delta t =  \int_{\mathcal{R}} \left(\frac{1}{c(\vec{x}(l))}-\frac{1}{c_{0}}\right)\d l = \int_{\mathcal{R}} s(\vec{x})-s_{0}\d l =\int_{\mathcal{R}} \delta s\d l
\end{equation}


\section{Straight Rays}

If one assume that straight and direct path between an emitter and a receiver is the fastest path, it assumes a homogeneous speed of sound along the ray.
The result of the reconstruction based on this approximation will be an average of each pixel-ray value, giving us a first good approximation of the speed of sound distribution, but lacking of dynamic range.
Determining the straight path can be done by the well-known Bresenham algorithm \cite{bresenham_algorithm_1965}.
The weighting of the ray can achieves in different ways according to the geometry of \autoref{fig:bresenham_dist}:
\begin{enumerate}
  \item $ m_{k,i} = a$
  \item $ m_{k,i}  =\frac{\|x_{r}-x_{r}\|}{n_{k}}$ , the total length is equally spread over the pixel path.
  \item $ m_{k,i} = l_{k,i}$.
\end{enumerate}
\begin{figure}[ht]
  \centering
  \input{tikz/2d_bresenham_dist.tikz}
  \caption{\label{fig:bresenham_dist} Exact distance of a ray in a pixel.}
\end{figure}

The length computed with the help of \autoref{fig:bresenham_dist} is more accurate, but computing it requires using heavy computation, and is 10 times slower for the reconstruction.
Considering a fraction of the overall length (2.) is a good compromise between speed and quality.

\section{Bent rays}

Bent ray approximation free us from assuming a constant speed of sound on the ray path.
The fastest way from an emitter to a receiver is not necessarily a straight path, by passing through pixel with higher speed of sound (and not to far from the straight ray approximation) the ray will arrive faster to destination.
Such path are called geodesics, and finding them is done using the fast marching algorithm.

\subsection{Fast Marching Map}

The fast marching method (FMM) is a design to solve the boundary value problem of the Eikonal equation.
The idea is to determine the time of arrival of a \emph{front} $\Gamma$ a close boundary.
This front moves at speed $c(\vec{x})$ so we have: $\nabla T$ = $1/c(x)$.
The initial condition states $T(\Gamma_{0}) = 0$.
Among the different methods, we used the \emph{multistencil Fast Marching (MSFM)} methods \cite{hassouna_multistencils_2007}, as it gives the most precise results.

As the FMM Map is unique for each transducer, we can save a lot of computing time by pre-computing those maps before iterating through the emitter-receiver pairs.

\begin{figure}[H]
  \centering
  \setlength{\mywidth}{.45\textwidth}
  \subcaptionbox{\label{fig:FMM_Map_time}Fast marching map}[0.55\linewidth]
    {\input{plot/FMM_map.tex}}%
  \subcaptionbox{\label{fig:FMM_Map_grad}Gradient of the fast marching map}[0.45\linewidth]
    {\input{plot/FMM_grad.tex}}
    \caption[Fast Marching Map]{\label{fig:FMM_Map} The Fast marching map gives the time of flight between every point and an emitter\subref{fig:FMM_Map_time}. The gradient of this map clearly shows the influence of a heterogeneous medium.}
\end{figure}

\FloatBarrier
\subsection{Path extraction}
For each emitter we thus compute this time of arrival map, and the construction of the geodesic path is done by searching a streamline FMM map.
For the weighting of the bent rays  different solution have been tested (with $r_{ki}$ the position of the $i^{th}$ pixel of the $k^{th}$ ray and $n_{k}$ the number of pixel in this ray)
\begin{enumerate}
  \item $ m_{k,i} = L_{k}/ n_{k}$, same as for straight rays.
  \item $ m_{k,i} = \frac{1}{n_{k}} \sum_{i=1}^{n_{k}-1}\|r_{k,i+1}-r_{k,i}\|_{2}$
  \item $ m_{k,i} = \|r_{k,i}-r_{k,i-1}\|$
  \item $ m_{k,i} = \frac{\hat{t}_{k}}{c_{k}n_{k}}$ where $\hat{t}_{k}$ is the estimated time of arrival extract from the FMM map.
\end{enumerate}

These different methods have been tested and only the number 1. and 4. of the length approximation have produced analysable results (\ie{} with a sufficient wide value range in the reconstructed image)

\section{Limits of Ray-Approximation}
\label{sec:phys_raylimits}
In the ray approximations (straight or bent) the maximum resolution is not dependent of the wave interference (diffraction and scattering) as in the Born approximation.
However, we can use results from geometrical optics.
If we don't rely on the infinite frequency hypothesis, the theoretical  resolution power is limited by the so called Fresnel-Zone.

Between the emitter and the receiver, there is some off-axis propagation (not on the line of sight).
This can deflect off of objects and then radiates to the receiver, adding to the direct path wave, in or out of phase.
The $n-$Fresnel zone is defined as the volume where the phasing between any reflected ray and the direct one is less than $n$ half-wavelength.
Such regions are by construction ellipsoid, as shown on \autoref{fig:fresnel_zone}.

In the first Fresnel zone, phase shift will be less than $\pi/2$, and indirect rays will be added constructively.
Considering those rays increases the power of the received signal.

\begin{figure}[ht]
  \centering
  \input{tikz/fresnel_zone.tikz}
  \caption[Interference in the fresnel zone]{\label{fig:fresnel_zone} Interference in the first Fresnel's zone. The direct path from emitter (E) to receiver (R) receive also interferences from the scatter points.}
\end{figure}
In a more formal way, for an emitter  $\vec{x_{e}}$ and a receiver $x_{r}$, any point $\vec{x}$ in the Fresnel zone, we have:

\begin{equation}
  \label{eq:Fresnel1}
  \|\vec{x}-\vec{x_{e}}\| + \|\vec{x_{r}}-\vec{x}\|  - \| \vec{x_{r}-\vec{x_{e}}} \|\le \frac{\lambda}{2}
\end{equation}

Because the Fresnel Zone is an ellipsoid, we can determine its radius as:
\begin{align*}
  d^{2} &= r_{f}^{2} + \left(\frac{L}{2}\right)\\
  r_{f}^{2} &= \left(\frac{L}{2}+\frac{\lambda}{4}\right)^{2}-\left( \frac{L}{2}\right) \\
d_{f} &= 2  r_{f} = \sqrt{L\lambda+\frac{\lambda^{2}}{4}} \overset{\lambda\ll L}{\simeq} \sqrt{L\lambda}
\end{align*}

In the case of the USCT, the A-scans have  an average frequency of \SI{2.5}{MHz} and a maximum speed of sound around \SI{1500}{m/s}, which gives us a wavelength $\lambda$  of \SI{0.6}{mm}.
The USCT 2 have an aperture diameter of \SI{27}{cm}, and we then have a width of \SI{12}{mm}, in the USCT 3 the aperture diameter is \SI{35}{cm}, having thus a higher region of interest (most of the signal overlap in the center of the aperture, where ideally the breast is fully positioned), in this case  we get \SI{14}{mm}.
One should  notice that this width is not the limit of the resolution power, but a range where the information about a ray can be spread.\cite{li_resolution_2013}


\section{Forward Model}

In our case the goal of the transmission tomography reconstruction is to obtain the speed of sound of each voxel in the breast, as it is a good estimator of the presence of tumorous cells (see \autoref{fig:cell_charts} and~\cite{greenleaf_clinical_1981}).
The measuring process and time detection can be expressed as a  linear operator (e.g. a matrix):

\begin{equation}
  \label{eq:F1}
  \vec{t} = \mathcal{M}(\vec{c})
\end{equation}

Where $\vec{t}$ is the measured data, here the time of arrival of a received puls, $\vec{M}$ is the measurement operator, and $\vec{c}$ the distribution of the speed of sound.
If $\mathcal{M}$ is  a linear operator, we can express it as a matrix $\vec{M}$  calls the measurement-matrix.
As we will work with the eikonal approximation, $\vec{M}$ is also named the \emph{ray-matrix}.

In fact, the Ray matrix is not independent of $\vec{c}$, as the propagation of the rays are determined by the speed of sound.
So we can proceed with an iterative reconstruction method:

\begin{enumerate}
  \item Compute $\mathcal{M}_{k}$ from $\vec{\hat{c}}_{k}$ with an method from \autoref{chap:ray}.
  \item Solve $ \vec{y} = \mathcal{M}_{k}(\vec{\hat{c}}_{k+1})$ with an algorithm from \autoref{chap:reco}.
  \item Stop reconstruction if $k>n_{max}$ or if the relative error is below tolerance, else increase $k$.
\end{enumerate}

The initial value of $\hat{\vec{c}}_{0}$ without further information is an homogeneous water filled background.
With such a scheme, the approximation of the forward model is more precise at each step, and converge to a final estimate $\hat{\vec{c}_{f}}$.


\begin{figure}[hbt]
  \centering
  \input{tikz/aperture_geom.tikz}
  \caption{\label{fig:aperture_geom} Aperture shape and space quantification}
\end{figure}

\begin{figure}[hbt]
  \centering
  \input{tikz/2d_ray2grid.tikz}
  \caption[Rays approximation in numerical approximation]{\label{fig:ray2grid}The different steps of the rays quantification}
\end{figure}

\section{Ray Matrix construction: Example of straight ray}

In the eikonal approximation, the linearization of equation \eqref{eq:Lin2} can be done relatively to the water background ($s_{0}=\frac{1}{c_{0}}$) if we consider no dampening across the ray:

\begin{equation}
  \label{eq:ray1}
  \delta t = \int_{\mathcal{R}}^{}\left(s(\vec{x})-s_{0}\right)\d l = \int_{\mathcal{R}}^{}\frac{1}{c(\vec{x})}-\frac{1}{c_{0}}\d l
\end{equation}

By considering a discrete space (see \autoref{fig:aperture_geom} and \autoref{fig:ray2grid}) we have for the $k$-th ray:

\begin{equation}
  \label{eq:ray2}
  \delta t_{k} = \sum_{i=1}^{n_{k}}l_{ki} \delta s_{i}
\end{equation}

Where $\delta s_{i}$ is the relative slowness in the $i^{th}$ voxel hit by the ray, and $l_{ki}$ the length of the $k^{th}$ ray in this voxel, and $n_{k}$ the number of voxels meet by the ray.
By considering all the rays and voxel we can build a linear system:

\begin{equation}
  \label{eq:ray3}
\underbrace{  \begin{bmatrix}
    t_{1} \\ t_{2} \\ \vdots \\ t_{M}
  \end{bmatrix}
}_{\vec{t}}   =
\underbrace{   \begin{bmatrix}
     l_{11} & l_{12} & \cdots & l_{1N} \\
     l_{21} & l_{22} & \cdots & l_{2N} \\
     \vdots      & \vdots      & \ddots    & \vdots     \\
     l_{M1} & l_{M2} & \cdots & l_{MN} \\
   \end{bmatrix}
 }_{\vec{L}}   \cdot
  \underbrace{ \begin{bmatrix}
      \frac{1}{c_{1}} \\ \frac{1}{c_{2}} \\ \vdots \\ \frac{1}{c_{N}}
   \end{bmatrix}
 }_{\vec{s}}
\end{equation}

From this system $\vec{t=Ls}$ (in the next chapter $\vec{y=Mx}$) we can express the  measure operator of the USCT for transmission tomography from equation~\eqref{eq:F1}:

\begin{equation}
  \label{eq:F2}
  \vec{t} = \underbrace{\vec{M}~\vec{\mathcal{I}}}_{\vec{\mathcal{M}}}(\vec{c})
\end{equation}

With $\mathcal{I}(c)$ the per-term inverse of $c$, which is a non-linear operator, but is conveniently an invertible one.
In practice the reconstruction software will use the linear decomposition $ t_{tot} = \delta t + t_{water}$ $s = \delta s + s_{0}$; and will only solve for the difference terms $ \delta \vec{t} = \vec{M} \delta \vec{s}$, and add the reference speed at the end.
Furthermore, the reference speed of sound in the water can be established as a function of the temperature distribution \cite{marczak_water_1997}, slightly improving the precision of the reconstructed image.


\section{Problem size}

As explained in \autoref{chap:phys}, the eikonal approximation, based on ray propagation is a good compromise between computation power and resolution power.
With this method, the smallest pixel size acceptable is 3 to 4 times the wavelength (so around \SI{2}{mm} wide pixel).
This yields to a choice of reconstruction volume of $128 \times 128 \times 96$ voxels, or a slice of $128\times 128$ in 2D, we have then \num{1572864} pixels to reconstruct.

\begin{figure}[hbt]
  \centering
  \input{tikz/2d_matrixbuild.tikz}
  \caption[Ray matrix construction]{\label{fig:matrixbuild} 2D example of matrix construction from a  straight ray. each row of the matrix corresponds to a specific ray, each column to a voxel}
\end{figure}

As measurement data we got $157\times4\times157\times9\times10=$\num{8873640} times of arrival from pair of emitters-receivers (with ten rotation of the aperture).
As presented in \autoref{sec:trans_resp} the transducer response is only good in a cone  \SI{30}{\degree} wide.

Only a third of the time picking is usable, we get \num{2957880} times of arrivals in this configuration.
Thus, the problem is over-determined, and we also know that noise could also disturb the time of flight picking.

In fact each ray gives only information on the voxel it has met, making  $\vec{M}$ a sparse matrix.
In a straight ray with 2D simulation we only get roughly 30\% voxels hits in the matrix. This result is much worse in 3D.

The construction of the matrix is done as described in \autoref{fig:matrixbuild}, where the reconstruction image is flattened into a vector.
Using CSC-format for saving it greatly reduces the memory usage, but the sparsity of the matrix also means that we lack information about pixels.
Having our rays covering more pixels adds information for the reconstruction. This fact will be the starting point of the different rays approximations we will discuss in \autoref{chap:fat}.

\section{Reducing the problem size}
As we have seen, the problem size is \emph{big}, especially in 3D.
But only a part of the reconstructed voxels is worth of interest, \ie{} the ones where we have breast cells (the rest being considered as water).
So it makes sense to only compute the reconstruction in this \emph{region of interest} (ROI).
By concentrating the computational power to this region we not only reduce the time needed for the reconstruction, but also discard the background noise, by considering all pixels outside the ROI  as water.

To determine this region I tried various image processing techniques to create a binary segmentation of the reconstructed image.
The best results (which are the most resilient to noise and a reduced number of transducers) are obtained by computing the gradient of the reconstructed image (via a Sobel Filter) and then used Otsu's Method  \cite{zhang_image_2008} on the joint histogram of the gradient and a Gaussian filtered one.
The gradient work as a good edge detector in our case, as the gradient of the noise background is much smaller as the one between the water and the cell.
Otsu's method allows us to compute an automatic and theoretically optimal threshold value for the binary segmentation, and frees us of manually tweaking  a threshold for each reconstructed image.
To remove the salt or pepper noise which could remain  from  the binarisation, I applied a median filter, and to reduce the risk of error and false detection, an error margin is fixed and apply through a dilatation filter.
Once a close border of the breast is determined, it is filled in, and we get the complete breast include in the ROI
The overall process is represented on \autoref{fig:seg_steps}.

However, more tests would be needed with experimental data, where artifacts could appear in the background and create therefore false detection.

\section{Summary}

The overall process of the Transmission Tomography is represented in the \autoref{fig:trans_scheme}.
From an initial guess on the reconstructed medium and a measured vector of time of arrival , we build a forward model, based on the eikonal equation.
This model is represented in the \emph{ray-matrix}, a linear operator between the relative slowness and the difference of time of arrival (with respect to an homogeneous water-filled background).

Using a smart reconstruction by concentrating the solving on a defined region of interest helps us to reduce the influence of the noise on the overall reconstruction.
The cost (time) of computing this region of interest is worth  it, as it allow us to greatly reduce the problem size (less pixels to reconstruct) and thus makes it faster to solve.

However, the sparse characteristic of the constructed matrix clearly shows that a simple ray tomography does not embrace all of the information available.
This implies the need of a large amount of rays (\ie numerous wide-opening-angle transducers) to be able to cover the complete aperture.

\begin{figure}[ht]
  \centering
  \foreach \seg in {1,2,3,4,5}{
    \begin{subfigure}{0.4\textwidth}
    \setlength{\fboxsep}{0pt}
    \fbox{\includegraphics[width=0.9\textwidth]{plot/seg\seg}}
    \caption{\label{fig:seg_steps\seg}}
  \end{subfigure}%
}
  \caption[Segmentations Steps]{\label{fig:seg_steps}
    Segmentations procedure:  the original image (\subref{fig:seg_steps1})  is differientated through Sobel's filter (\subref{fig:seg_steps2}), then binarized with Otsu's method (\subref{fig:seg_steps3}), and median filter (\subref{fig:seg_steps4}). In the end(\subref{fig:seg_steps5}) we get the boundary of the breast (blue), a safe zone to reconstruct (where the real breast should be,dash line) and the bounding box (red) of the reconstruction volume (where the inverse problem will be solved).
  }
\end{figure}

\begin{sidewaysfigure}
  \centering
    \resizebox{!}{0.32\linewidth}{
      \input{tikz/trans_schematics.tikz}
    }
  \caption[Global transmission tomography process]{\label{fig:trans_scheme} Transmission tomography Process, the black part of filter are cut off, the white is conserved and the gray is considered as water.}
\end{sidewaysfigure}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
