%% LaTeX-Beamer poster template for KIT design
%% by Erik Burger, Christian Hammer
%%
%% version 1.2
%%
%% mostly compatible to KIT corporate design v1.2
%% http://www.uni-karlsruhe.de/download/uka/Gestaltungsrichtlinien_komplett.pdf
%%
%% Problems, bugs and comments to
%% burger@kit.edu

\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{mathrsfs}
\usepackage{amssymb}
\usepackage{amsbsy}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\usepackage{siunitx}
\usepackage{caption}
\usepackage{qrcode}
\newlength{\mywidth}
\newlength{\myheight}
\usetikzlibrary{
  backgrounds,
  positioning,
  decorations.pathreplacing,
  calc,
}
\usepackage{bm}
\input{sty/parula}
%% Fill in the page size here. If the proportions of the fonts
%% are not satisfactory, change the scale parameter
\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\usepackage{sty/beamerthemekit}
\usepackage{sty/beamerthemekitposter}
% \usepackage{sty/semirounded}
% \useinnertheme{rounded}
\title{Analysis and optimization \\[.4em]  of ray-based 3D Ultrasound Tomography}
\subtitle{Workshop ARPE 2020}
\author{Pierre-Antoine Comby, Torsten Hopp, Erich Vourc'h}

\institute{\textbf{USCT3D Project\\[.2em]
    Institute for Data Processing and Electronic (IPE)}\\[.4em]
  Building 242 Hermann-von-Helmholtz-Platz 1\\[.4em]
  D-76344 Eggenstein-Leopoldshafen
  \href{http://www.ipe.kit.edu}{http://www.ipe.kit.edu}
}
\setlength{\kitradius}{1em}
\setbeamertemplate{block begin}{
  \vskip.75ex
  \begin{beamerboxessemirounded}[upper=block title,lower=block body]%
    % \raisebox{0pt}[3.5ex][-0.5ex]{%
    {\raisebox{.5\height}[3ex][0ex]{\usebeamerfont*{block title}\hspace{15pt}\insertblocktitle}}
    % }%
    \raggedright\leftskip=40pt
    \usebeamerfont{block body}%
  }
  \setbeamertemplate{block end}{
  \end{beamerboxessemirounded}\vskip\smallskipamount
}

\begin{document}
% change the following line to "ngerman" for German style date and logos
\begin{frame}[t]
  \selectlanguage{english}
  % \vspace{-10em}
  \begin{columns}
    \begin{column}{.4\textwidth}
      \begin{block}{Abstract}
        By probing the acoustic characteristic of a women breast (speed of sound, attenuation, reflectivity), one can detect tumour in their early stage, maximizing the chance of cure.
        The probe is done by measuring time of flight of ultrasound pulses inside a water-filled aperture, and the speed of sound reconstruction (transmission tomography) is optimized, by developing ray-based approaches to state and solve the inverse problem. The reconstruction can later be used to produce multi-modal imaging or enhance the reflexion tomography reconstruction.

        New ray methods are developed and tested on a 2D simulation phantom, providing reproducibility and ease of prototyping. The use of 3D is motivated as  all scattered information (eg. out of plane) will be retrieved.
      \end{block}
    \end{column}
    \begin{column}{.6\textwidth}
      \begin{block}{3D USCT }
        \begin{columns}
          \begin{column}{0.45\textwidth}
            \centering
            \includegraphics[width=0.9\textwidth]{img/usct3.png}%
          \end{column}
          \begin{column}{.55\textwidth}
            \begin{itemize}
              \item Detect Breast tumour in early stage (\(<\) 5mm)
              \item Ultrasonic based: cheaper, safer, ideally better resolution than CT and MRI
              \item Probes velocity, attenuation, reflectivity
            \end{itemize}
          \end{column}
        \end{columns}
      \end{block}
      \begin{block}{Base principle}
        \begin{columns}
          \begin{column}{.5\textwidth}
            \resizebox{0.49\textwidth}{!}{\input{tikz/2d-acquisition.tikz}}%
            \resizebox{0.49\textwidth}{!}{\input{plot/Ascan_rec_sep2.tex}}
          \end{column}
          \begin{column}{.5\textwidth}
            The time of flight of the transmission pulse (blue) is affected by the slowness of the medium it crosses: \(\bm{t = \int \frac{1}{c} dl}\). Determing the ray (fastest path) is needed to compute the speed of sound.
          \end{column}
        \end{columns}
      \end{block}
    \end{column}
  \end{columns}
  \setlength{\mywidth}{0.11\textwidth}
  \setlength{\myheight}{\mywidth}
  \begin{columns}
    \begin{column}{.66\textwidth}
      \begin{block}{Rays Methods}
        \begin{columns}
          \begin{column}{.49\linewidth}
            \begin{exampleblock}{1. Straight rays}
              \begin{columns}
                \begin{column}{.33\textwidth}
                  \resizebox{\mywidth}{!}{\tiny\input{tikz/2d_straightray.tikz}}
                \end{column}
                \begin{column}{.66\textwidth}
                  \begin{itemize}
                    \item homogeneous \emph{a priori}
                    \item Bresenham's Line Algorithm
                    \item Very Fast and Easy (C-Mex)
                    \item Noise sensible
                    \item 99\% sparse
                  \end{itemize}
                \end{column}
              \end{columns}
            \end{exampleblock}
            \begin{exampleblock}{2. Bent rays}
              \begin{columns}
                \begin{column}{.33\textwidth}
                  \resizebox{\mywidth}{!}{\tiny\input{tikz/2d_bentray.tikz}}
                \end{column}
                \begin{column}{.66\textwidth}
                  \begin{itemize}
                    \item heterogeneous \emph{a priori}
                    \item theory of ray optics
                    \item Fastest \(\neq\) Shortest path
                    \item Fast Marching Map
                    \item Slow, better results
                  \end{itemize}
                \end{column}
              \end{columns}
            \end{exampleblock}
          \end{column}
          \begin{column}{.49\linewidth}
            \begin{exampleblock}{3. Fat rays}
              \begin{itemize}
                \item Finite Frequency (better with broadband)
                \item Geophysics Theory (Fréchet Kernel)
                \item consider all constructives interferences insides the Fresnel Zone
                \item Reduce sparsity and noise sensibility.
              \end{itemize}
              \[
                \bm{t} =  \int \bm{K}(\bm{x})\frac{1}{c(\bm{x})} \text{d}\bm{x}
              \]
              \setlength{\mywidth}{\linewidth}
              \setlength{\myheight}{0.3\mywidth}
              \hspace{-1em}\input{plot/kernel/kernel_comp2.tex}
            \end{exampleblock}
          \end{column}
        \end{columns}
      \end{block}
    \end{column}
    \begin{column}{0.34\textwidth}
      \begin{block}{Inverse Problem Solver}
        \begin{itemize}
          \item Reconstruction is done on relative slowness (\(\delta s=1/c-1/c_{0}\)) to have a linear system
          \item ill posed and ill-conditioned
          \item Total Variation (TV) Regularization: Consider a sparse Gradient (piecewise-like distribution of speed of sound)
          \item Two solver tested
            \begin{itemize}
              \item SART
                \[\scalebox{1.3}{\ensuremath{\vec{x}^{(k+1)} = \vec{x}^{(k)}+ \lambda \vec{C}\vec{M}^{T}\vec{R}(\vec{y}-\vec{M}\vec{x}^{(k)})}}
                \]
              \item TVAL3 (TV regularization included)
            \end{itemize}
                \[\scalebox{1.1}{\ensuremath{
                \min \|\vec{D_{x}x}\|_{1}+\|\vec{D_{y}x}\|_{1}+\|\vec{D_{z}x}\|_{1} \text{ with } \vec{y}=\vec{Mx}
                }}\]
          \item Use of a ROI
            \begin{itemize}
              \item Better and Faster, mandatory in 3D (memory explosion)
              \item Auto detection after first guess.
            \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{block}{Results}
    \begin{columns}
      \begin{column}{.7\textwidth}
        \centering
        \begin{columns}
          \begin{column}{.5\textwidth}
            \centering
            \includegraphics[width=0.95\linewidth]{plot/beta_mu/beta-2_mu+3.png}\\
            TVAL Strong Regularisation
          \end{column}
          \begin{column}{.5\textwidth}
            \centering
            \includegraphics[width=0.95\linewidth]{plot/sart_180_1_all.png}\\
            SART, low Backgroung noise
          \end{column}
        \end{columns}
      \end{column}
      \begin{column}{.3\textwidth}
        \centering
        \setlength{\myheight}{10em}
        \includegraphics[height=\myheight]{plot/3dx.png}%
        \includegraphics[height=\myheight]{plot/3dz.png}\\
        \begin{minipage}[t]{0.9\linewidth}
          3D reconstruction using Fréchet Kernel: Phantom is ball containing an off-center anomality (SART+TV+ROI)
        \end{minipage}
      \end{column}
    \end{columns}
  \end{block}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{block}{Conclusion}
        The newly developed Fat ray models allows better gathering of information and increases SNR, but with a small additional cost in computation, that could be overcome with a multi-threaded and low-level approach. The Infinite frequency methods (straight and bent rays) are outworn in 3D. The high sparsity of the built system is overcome by using Total Variation regularization. No optimal tuning for the TVAL could be found, SART+TV produces as least as good results and are more predictable.
      \end{block}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{block}{Main References}
        \begin{columns}
          \begin{column}{.75\textwidth}
            \begin{itemize}
              \item{}[1] HOPP T., ZUCH F., COMBY P-A., et al.\emph{Fat ray ultrasound transmission tomography: preliminary experimental results with simulated data.} % In : Medical Imaging 2020: Ultrasonic Imaging and Tomography. International Society for Optics and Photonics, 2020. p. 113190M.
              \item{}[2] GEMMEKE H., HOPP T., ZAPF M., et al.\emph{3D ultrasound computer tomography: Hardware setup, reconstruction methods and first clinical results.}  % Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment, 2017, vol. 873, p. 59-65.
            \end{itemize}
          \end{column}
          \begin{column}{.25\textwidth}
            \centering
            Report, Talk,\\ and more ... \\[.4em]
            \qrcode[height=0.8\linewidth]{https://perso.crans.org/comby/ipe}
          \end{column}
        \end{columns}
        \vspace{1em}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}
\end{document}
